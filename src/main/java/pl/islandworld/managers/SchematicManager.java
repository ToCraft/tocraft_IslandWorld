package pl.islandworld.managers;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import pl.islandworld.IslandWorld;
import pl.islandworld.entity.SimpleIsland;

import java.io.File;

public abstract class SchematicManager {
    @SuppressWarnings("unused")
    private static IslandWorld plugin;

    public void pasteSchematic(World world, File file, Location origin) {
    }

    public void pasteSchematic(Player player, SimpleIsland island, String sName) {
    }
}