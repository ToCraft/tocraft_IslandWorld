package pl.islandworld.database;

import pl.islandworld.IslandWorld;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MySQL extends Database {
    private String user = "";
    private String database = "";
    private String password = "";
    private String port = "";
    private String hostname = "";
    private Connection c = null;
    private IslandWorld plugin = null;
    private Boolean quiet = false;

    public MySQL(IslandWorld plugin, String hostname, String portnmbr, String database, String username, String password, Boolean quiet) {
        this.hostname = hostname;
        this.port = portnmbr;
        this.database = database;
        this.user = username;
        this.password = password;
        this.plugin = plugin;
        this.quiet = quiet;
    }

    public Connection open() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            this.c = DriverManager.getConnection("jdbc:mysql://" + this.hostname + ":" + this.port + "/" + this.database, this.user, this.password);
            return c;
        } catch (SQLException e) {
            if (!quiet)
                plugin.ShowWarn("Could not connect to MySQL server! because:" + e.getMessage());
        } catch (ClassNotFoundException e) {
            if (!quiet)
                plugin.ShowWarn("JDBC Driver not found!");
        }
        return this.c;
    }

    public boolean checkConnection() {
        return this.c != null;
    }

    public Connection getConn() {
        return this.c;
    }

    public void closeConnection(Connection c) {
        c = null;
    }
}
