package pl.islandworld.listeners;

import org.bukkit.Location;
import org.bukkit.entity.Horse;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.InventoryHolder;
import pl.islandworld.IslandWorld;


/**
 * @author Gnacik
 */
@SuppressWarnings({"unused", "SpellCheckingInspection"})
public class HorseInvListener implements Listener {
    private final IslandWorld plugin;

    public HorseInvListener(IslandWorld iw) {
        this.plugin = iw;
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onInventoryOpenEvent(InventoryOpenEvent event) {
        if (event.isCancelled() || event.getInventory() == null || event.getInventory().getHolder() == null)
            return;

        final Player player = (Player) event.getPlayer();
        final InventoryHolder h = event.getInventory().getHolder();

        if (h instanceof Horse) {
            if (player.getWorld() != IslandWorld.getIslandWorld())
                return;
            if (player.hasPermission("islandworld.bypass.island"))
                return;

            Location loc = player.getLocation();
            if (event.getInventory().getHolder() instanceof Horse) {
                final Horse hor = (Horse) event.getInventory().getHolder();
                loc = hor.getLocation();
            }

            if (!plugin.canBuildOnLocation(player, loc))
                event.setCancelled(true);
        }
    }
}