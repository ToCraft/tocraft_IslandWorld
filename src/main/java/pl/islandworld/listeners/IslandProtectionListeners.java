package pl.islandworld.listeners;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.*;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.player.*;
import org.bukkit.event.vehicle.VehicleDamageEvent;
import org.bukkit.event.vehicle.VehicleEnterEvent;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import pl.islandworld.Config;
import pl.islandworld.IslandWorld;
import pl.tocraft.core.CorePlugin;

/**
 * @author Gnacik & artur9010
 */
@SuppressWarnings("unused")
public class IslandProtectionListeners implements Listener {
    private final IslandWorld plugin;

    public IslandProtectionListeners(IslandWorld iw) {
        this.plugin = iw;
    }

    public static boolean isDigit(String text) {
        if ((text == null) || text.isEmpty()) {
            return false;
        }
        for (char c : text.toCharArray()) {
            if (!Character.isDigit(c)) {
                return false;
            }
        }
        return true;
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerBlockBreak(BlockBreakEvent event) {
        if (event.isCancelled())
            return;

        final Player player = event.getPlayer();
        final Block block = event.getBlock();

        if (player.getWorld() != IslandWorld.getIslandWorld())
            return;
        if (player.hasPermission("islandworld.bypass.island"))
            return;

        if (block != null && !plugin.canBuildOnLocation(player, block.getLocation())){
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerBlockPlace(BlockPlaceEvent event) {
        if (event.isCancelled())
            return;

        final Player player = event.getPlayer();
        final Block block = event.getBlock();

        if (player.getWorld() != IslandWorld.getIslandWorld())
            return;
        if (player.hasPermission("islandworld.bypass.island"))
            return;

        if (block != null && !plugin.canBuildOnLocation(player, block.getLocation())){
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onItemDrop(PlayerDropItemEvent event) {
        if (event.isCancelled())
            return;

        final Player player = event.getPlayer();
        final Item item = event.getItemDrop();

        if (item == null)
            return;
        if (player.getWorld() != IslandWorld.getIslandWorld())
            return;
        if (player.hasPermission("islandworld.bypass.island"))
            return;

        if (!(plugin.canBuildOnLocation(player, item.getLocation()))){
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onInventoryOpenEvent(InventoryOpenEvent event) {
        if (event.isCancelled() || event.getInventory() == null || event.getInventory().getHolder() == null)
            return;

        final Player player = (Player) event.getPlayer();
        final InventoryHolder h = event.getInventory().getHolder();

        if (h instanceof Chest || h instanceof DoubleChest || h instanceof Furnace || h instanceof Dispenser) {
            if (player.getWorld() != IslandWorld.getIslandWorld())
                return;
            if (player.hasPermission("islandworld.bypass.island"))
                return;

            Location loc = player.getLocation();
            if (event.getInventory().getHolder() instanceof Chest) {
                final Chest chest = (Chest) event.getInventory().getHolder();
                loc = chest.getLocation();
            } else if (event.getInventory().getHolder() instanceof Furnace) {
                final Furnace fur = (Furnace) event.getInventory().getHolder();
                loc = fur.getLocation();
            } else if (event.getInventory().getHolder() instanceof DoubleChest) {
                final DoubleChest chest = (DoubleChest) event.getInventory().getHolder();
                loc = chest.getLocation();
            } else if (event.getInventory().getHolder() instanceof Dispenser) {
                final Dispenser disp = (Dispenser) event.getInventory().getHolder();
                loc = disp.getLocation();
            }

            if (!(plugin.canBuildOnLocation(player, loc))){
                event.setCancelled(true);
            }
        }

    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onRightHandPlayerInteact(PlayerInteractEvent event) {
        if (event.isCancelled())
            return;

        final Player player = event.getPlayer();

        if (player != null && (event.getAction() == Action.RIGHT_CLICK_BLOCK || event.getAction() == Action.RIGHT_CLICK_AIR)) {
            Location loc = player.getLocation();
            // If block is not null use block location, if null use player loc
            final Block block = event.getClickedBlock();
            if (block != null)
                loc = block.getLocation();

            if (player.getWorld() != IslandWorld.getIslandWorld())
                return;
            if (player.hasPermission("islandworld.bypass.island"))
                return;

            if (player.getItemInHand().getType() == Material.MONSTER_EGG) {
                if (!(plugin.canBuildOnLocation(player, loc))){
                    event.setCancelled(true);
                }
            } else if (player.getItemInHand().getType() == Material.FIREBALL) {
                if (!plugin.canBuildOnLocation(player, loc)){
                    event.setCancelled(true);
                }
            }
        }
    }

    @SuppressWarnings("deprecation")
    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerInteract(PlayerInteractEvent event) {
        if (event.isCancelled())
            return;

        final Player player = event.getPlayer();
        final Block block = event.getClickedBlock();
        final ItemStack item = event.getItem();

        if (player.getWorld() != IslandWorld.getIslandWorld())
            return;
        if (player.hasPermission("islandworld.bypass.island"))
            return;

        if (block != null) {
            final Material t = block.getType();

            if (!(plugin.getConfig().getBoolean("flags.use-switches", false) && isSwitch(t))) {
                if (!(isOnAllowedList(t) || plugin.canBuildOnLocation(player, block.getLocation()))){
                    event.setCancelled(true);
                }
            }
        } else {
            if (item != null) {
                if (!(isOnAllowedList(item.getType()) || plugin.canBuildOnLocation(player, player.getLocation()))){
                    event.setCancelled(true);
                }
            }
        }
    }

    public boolean isSwitch(Material m) {
        return (m == Material.WOOD_BUTTON || m == Material.STONE_BUTTON || m == Material.LEVER || m == Material.WOOD_PLATE || m == Material.STONE_PLATE);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerInteractEntity(PlayerInteractEntityEvent event) {
        if (event.isCancelled())
            return;

        final Player player = event.getPlayer();
        final Entity e = event.getRightClicked();

        if (player.getWorld() != IslandWorld.getIslandWorld())
            return;
        if (player.hasPermission("islandworld.bypass.island"))
            return;

        if (e instanceof Vehicle) {
            if (!plugin.canBuildOnLocation(player, e.getLocation())){
                event.setCancelled(true);
            }
        } else {
            if (e != null && !plugin.canBuildOnLocation(player, e.getLocation())){
                event.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerShearEntity(PlayerShearEntityEvent event) {
        if (event.isCancelled())
            return;

        final Player player = event.getPlayer();
        final Entity entity = event.getEntity();

        if (player.getWorld() != IslandWorld.getIslandWorld())
            return;
        if (player.hasPermission("islandworld.bypass.island"))
            return;

        if (entity != null && !plugin.canBuildOnLocation(player, entity.getLocation())){
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerBucketFill(PlayerBucketFillEvent event) {
        if (event.isCancelled())
            return;

        final Player player = event.getPlayer();
        final Block block = event.getBlockClicked();

        if (player.getWorld() != IslandWorld.getIslandWorld())
            return;
        if (player.hasPermission("islandworld.bypass.island"))
            return;

        if (block != null && !plugin.canBuildOnLocation(player, block.getLocation())){
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerBucketEmpty(PlayerBucketEmptyEvent event) {
        if (event.isCancelled())
            return;

        final Player player = event.getPlayer();
        final Block block = event.getBlockClicked();

        if (player.getWorld() != IslandWorld.getIslandWorld())
            return;
        if (player.hasPermission("islandworld.bypass.island"))
            return;

        if (block != null && !plugin.canBuildOnLocation(player, block.getLocation())){
            event.setCancelled(true);
        }

    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onVehicleEnter(VehicleEnterEvent event) {
        if (event.isCancelled())
            return;

        if (event.getEntered() instanceof Player) {
            final Player player = (Player) event.getEntered();
            final Vehicle vehicle = event.getVehicle();

            if (player.getWorld() != IslandWorld.getIslandWorld())
                return;
            if (player.hasPermission("islandworld.bypass.island"))
                return;

            if (vehicle != null && !(plugin.canBuildOnLocation(player, vehicle.getLocation()))){
                event.setCancelled(true);
            }

        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onVehicleDamage(VehicleDamageEvent event) {
        if (event.isCancelled())
            return;

        if (event.getAttacker() instanceof Player) {
            final Player player = (Player) event.getAttacker();
            final Vehicle vehicle = event.getVehicle();

            if (player.getWorld() != IslandWorld.getIslandWorld())
                return;
            if (player.hasPermission("islandworld.bypass.island"))
                return;

            if (vehicle != null && !plugin.canBuildOnLocation(player, vehicle.getLocation())){
                event.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerPickupItemEvent(PlayerPickupItemEvent event) {
        if (event.isCancelled())
            return;

        final Player player = event.getPlayer();
        final Item item = event.getItem();

        if (player.getWorld() != IslandWorld.getIslandWorld())
            return;
        if (player.hasPermission("islandworld.bypass.island"))
            return;

        if (item != null &&  !plugin.canBuildOnLocation(player, item.getLocation()))
            event.setCancelled(true);

    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onProjectileLaunchEvent(ProjectileLaunchEvent event) {
        if (event.isCancelled())
            return;

        if (event.getEntity().getShooter() instanceof Player && event.getEntity() instanceof ThrownPotion) {
            final Player player = (Player) event.getEntity().getShooter();

            if (player.getWorld() != IslandWorld.getIslandWorld())
                return;
            if (player.hasPermission("islandworld.bypass.island"))
                return;

            if (!plugin.canBuildOnLocation(player, player.getLocation())){
                event.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
        if (event.isCancelled())
            return;

        final Entity target = event.getEntity();

        Player attacker = null;
        if (event.getDamager() instanceof Player) {
            attacker = (Player) event.getDamager();
        } else if (event.getDamager() instanceof Projectile) {
            if (((Projectile) event.getDamager()).getShooter() instanceof Player)
                attacker = (Player) ((Projectile) event.getDamager()).getShooter();
        }

        if (target != null && attacker != null) {
            if (attacker.getWorld() != IslandWorld.getIslandWorld())
                return;
            if (attacker.hasPermission("islandworld.bypass.damage"))
                return;

            if (target instanceof Player) {
                event.setCancelled(true);
            } else {
                if (target instanceof ItemFrame) {
                    if (!plugin.canBuildOnLocation(attacker, target.getLocation())){
                        event.setCancelled(true);
                    }
                } else if (target instanceof Animals || target instanceof NPC) {
                    if (!plugin.canBuildOnLocation(attacker, target.getLocation())) {
                        event.setCancelled(true);
                    }
                }
            }
        }
    }

    @SuppressWarnings("deprecation")
    private boolean isOnAllowedList(Material t) {
        if (Config.ALLOWED_ITEM_LIST != null && !Config.ALLOWED_ITEM_LIST.isEmpty()) {
            for (String blockDescr : Config.ALLOWED_ITEM_LIST) {
                if (blockDescr == null || blockDescr.isEmpty())
                    continue;

                String[] split = blockDescr.split(":");

                if (split.length > 0 && isDigit(split[0]) && Integer.parseInt(split[0]) == t.getId()) {
                    return true;
                }
            }
        }

        return false;
    }
}