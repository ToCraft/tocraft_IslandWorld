package pl.islandworld.listeners;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import pl.islandworld.Config;
import pl.islandworld.IslandWorld;
import pl.islandworld.entity.SimpleIsland;
import pl.tocraft.core.CorePlugin;

import java.util.HashMap;

@SuppressWarnings("unused")
public class PlayerMoveListener implements Listener {
    private final IslandWorld plugin;

    private HashMap<String, String> curentPos = new HashMap<>();

    public PlayerMoveListener(IslandWorld iw) {
        this.plugin = iw;
    }

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent event) {
        final Player player = event.getPlayer();

        if (player == null || event.isCancelled())
            return;

        if (event.getFrom().getWorld() == IslandWorld.getIslandWorld()) {
            if (event.getFrom().getBlockX() != event.getTo().getBlockX() || event.getFrom().getBlockY() != event.getTo().getBlockY() || event.getFrom().getBlockZ() != event.getTo().getBlockZ())
                checkCoords(player, event.getTo());
        }
    }

    @EventHandler
    public void onPlayerTeleport(PlayerTeleportEvent event) {
        final Player player = event.getPlayer();

        if (player == null || event.isCancelled())
            return;

        checkCoords(player, event.getTo());
    }

    private void checkCoords(Player player, Location loc) {
        final int blockX = loc.getBlockX();
        final int blockZ = loc.getBlockZ();
        final World w = loc.getWorld();

        final String plName = player.getName().toLowerCase();
        final String coordHash = String.valueOf(blockX / Config.ISLE_SIZE) + "-" + String.valueOf(blockZ / Config.ISLE_SIZE);
        final SimpleIsland is = plugin.getCoordList().get(coordHash);

        if (is != null && w == IslandWorld.getIslandWorld() && plugin.isInsideIsland(loc, is)) {
            if (!curentPos.containsKey(plName) || (curentPos.containsKey(plName) && !curentPos.get(plName).equalsIgnoreCase(coordHash))) {
                CorePlugin.sendPrefixed(player, "Wchodzisz na wyspe gracza &6" + is.getOwner());
                curentPos.put(plName, coordHash);
                return;
            }
        }

        if (curentPos.containsKey(plName) && (is == null || w != IslandWorld.getIslandWorld() || (!plugin.isInsideIsland(player, is) || !curentPos.get(plName).equalsIgnoreCase(coordHash)))) {
            CorePlugin.sendPrefixed(player, "Opusciles wyspe.");
            curentPos.remove(plName);
        }
    }
}