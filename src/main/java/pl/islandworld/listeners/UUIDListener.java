package pl.islandworld.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import pl.islandworld.IslandWorld;
import pl.islandworld.entity.SimpleIsland;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;


/**
 * @author Gnacik
 */
public class UUIDListener implements Listener {
    private final IslandWorld plugin;

    public UUIDListener(IslandWorld iw) {
        this.plugin = iw;
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerJoin(PlayerJoinEvent event) {
        final Player player = event.getPlayer();

        if (player != null) {
            final String plName = player.getName();
            final UUID uuId = player.getUniqueId();
            final SimpleIsland island = plugin.getPlayerIsland(player);

            if (island != null) {
                if (island.getOwnerUUID() == null)
                    island.setOwnerUUID(player.getUniqueId());
            } else {
                final SimpleIsland isbyUID = plugin.getUUIDList().get(uuId);
                if (isbyUID != null) {
                    Map<String, SimpleIsland> isList = new HashMap<>();
                    isList.putAll(plugin.getIsleList());

                    String oldowner = null;
                    for (Entry<String, SimpleIsland> e : isList.entrySet()) {
                        final String owner = e.getKey();
                        final SimpleIsland isTemp = e.getValue();

                        if (isTemp == isbyUID) {
                            oldowner = owner;
                            break;
                        }
                    }

                    if (oldowner != null) {
                        // Remove island from list
                        plugin.getIsleList().remove(oldowner);
                        // Change owner
                        isbyUID.setOwner(plName);
                        // Add island into list
                        plugin.getIsleList().put(plName.toLowerCase(), isbyUID);
                        // Save
                        try {
                            plugin.saveDatFiles();
                        } catch (Exception ignored) {}
                    }
                }

            }
        }
    }
}