package pl.islandworld.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import pl.themolka.cmds.command.Command;
import pl.themolka.cmds.command.CommandException;

/**
 * Created by Artur on 07.09.2015.
 */
public class RemoveCommand extends Command {

    public RemoveCommand() {
        super(new String[]{"remove", "wyrzuc"});
        super.setDescription("Usuwa gracza z wyspy.");
    }

    @Override
    public void handle(CommandSender sender, String label, String[] args) throws CommandException {
        StringBuilder sb = new StringBuilder();
        for (String s : args){
            sb.append(s);
            sb.append(" ");
        }
        Bukkit.getServer().dispatchCommand(sender, "is remove " + sb.toString());
    }
}
