package pl.islandworld.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import pl.themolka.cmds.command.Command;
import pl.themolka.cmds.command.CommandException;

/**
 * Created by Artur on 07.09.2015.
 */
public class LeaveCommand extends Command {

    public LeaveCommand() {
        super(new String[]{"leave"});
        super.setDescription("Opusc wyspe.");
    }

    @Override
    public void handle(CommandSender sender, String label, String[] args) throws CommandException {
        Bukkit.getServer().dispatchCommand(sender, "is leave");
    }
}
