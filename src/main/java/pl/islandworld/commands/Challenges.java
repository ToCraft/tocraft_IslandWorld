package pl.islandworld.commands;

import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import pl.islandworld.IslandWorld;
import pl.themolka.cmds.command.CommandException;
import pl.tocraft.core.CorePlugin;

public class Challenges extends pl.themolka.cmds.command.Command {
    private final IslandWorld plugin;

    public Challenges() {
        super(new String[]{"challanges", "cha", "zadania", "zadanie"});
        super.setDescription("Zadania");
        this.plugin = IslandWorld.getInstance();
    }

    @Override
    public void handle(CommandSender sender, String label, String[] args) throws CommandException {
        final Player player = (Player) sender;

		if (args.length == 0)
		{
			showUsage(player);
		}
		else if (args.length == 1)
		{
			if (args[0].equalsIgnoreCase("list"))
			{
				commandList(player, "0");
			}
			else if (args[0].equalsIgnoreCase("complete"))
			{
				commandComplete(player, null);
			}
		}
		else if (args.length == 2)
		{
			if (args[0].equalsIgnoreCase("list"))
			{
				commandList(player, args[1]);
			}
			else if (args[0].equalsIgnoreCase("complete"))
			{
				commandComplete(player, args[1]);
			}
		}
		else
		{
			showUsage(player);
		}

    }

    private void commandComplete(Player player, String idnum) {
        if (player == null)
            return;

        if (idnum == null || idnum.isEmpty()) {
            CorePlugin.sendPrefixed(player, "Poprawne uzycie:");
            CorePlugin.send(player, "/zadania complete [id]");
            return;
        }

        if(!plugin.isDigit(idnum)){
            CorePlugin.sendPrefixed(player, "Niepoprawne ID zadania.");
            return;
        }

        plugin.completeChallenge(player, Integer.valueOf(idnum));
    }

    private void commandList(Player player, String ff) {
        if (player == null)
            return;

        int from = 0;
        if (plugin.isDigit(ff))
            from = Integer.valueOf(ff);

        plugin.showChallengeList(player, player.getName().toLowerCase(), from);
    }

    private void showUsage(Player player) {
        player.chat("/island"); //tymczasowe rozwiazanie :>
    }

}