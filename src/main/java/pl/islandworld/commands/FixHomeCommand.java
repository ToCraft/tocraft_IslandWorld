package pl.islandworld.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import pl.themolka.cmds.command.Command;
import pl.themolka.cmds.command.CommandException;

/**
 * Created by Artur on 07.09.2015.
 */
public class FixHomeCommand extends Command {

    public FixHomeCommand() {
        super(new String[]{"fixhome", "naprawdom"});
        super.setDescription("Poprawia punkt w ktorym znajduje sie home.");
    }

    @Override
    public void handle(CommandSender sender, String label, String[] args) throws CommandException {
        Bukkit.getServer().dispatchCommand(sender, "is fixhome");
    }
}
