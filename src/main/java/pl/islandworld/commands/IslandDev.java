package pl.islandworld.commands;

import org.apache.commons.lang.StringUtils;
import org.bukkit.*;
import org.bukkit.block.Biome;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.InventoryHolder;
import pl.islandworld.Config;
import pl.islandworld.IslandWorld;
import pl.islandworld.WeApi;
import pl.islandworld.entity.Challenge;
import pl.islandworld.entity.MyLocation;
import pl.islandworld.entity.SimpleIsland;
import pl.themolka.cmds.command.Command;
import pl.themolka.cmds.command.CommandException;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

/**
 * @author Gnacik
 */
public class IslandDev extends Command {
    private final IslandWorld plugin;

    public IslandDev() {
        super(new String[]{"islanddev", "isdev"});
        super.setDescription("Komendy wyspy (admin)");
        super.setPermission("islandworld.islandev");

        this.plugin = IslandWorld.getInstance();
    }

    @Override
    public void handle(CommandSender sender, String label, String[] args) throws CommandException {
        final World world = plugin.getIslandWorld();

        if (args.length == 0) {
            showUsage(sender);
        } else if (args.length == 1) {
            if (args[0].equalsIgnoreCase("reload")) {
                commandReload(world, sender);
            } else if (args[0].equalsIgnoreCase("unload")) {
                commandUnload(world, sender);
            } else if (args[0].equalsIgnoreCase("save")) {
                commandSave(world, sender);
            } else if (args[0].equalsIgnoreCase("visitclear")) {
                commandVisitClear(world, sender);
            } else if (args[0].equalsIgnoreCase("chaclear")) {
                commandChallengesClear(world, sender);
            } else if (args[0].equalsIgnoreCase("chadel")) {
                commandChallengesDelete(world, sender, null);
            } else if (args[0].equalsIgnoreCase("info")) {
                commandInfo(world, sender, null);
            } else if (args[0].equalsIgnoreCase("delete")) {
                commandDelete(world, sender, null);
            } else if (args[0].equalsIgnoreCase("tp")) {
                commandTeleport(world, sender, null);
            } else if (args[0].equalsIgnoreCase("sethome")) {
                commandSetHome(world, sender, null);
            } else if (args[0].equalsIgnoreCase("fixhome")) {
                commandFixHome(world, sender, null);
            } else if (args[0].equalsIgnoreCase("purge")) {
                commandPurge(world, sender, null, null);
            } else if (args[0].equalsIgnoreCase("purgebreak")) {
                commandPurgeBreak(world, sender);
            } else if (args[0].equalsIgnoreCase("challenges")) {
                commandChallenges(world, sender, "", "");
            } else if (args[0].equalsIgnoreCase("cleardrop")) {
                commandClearDrop(world, sender);
            } else if (args[0].equalsIgnoreCase("create")) {
                commandCreate(world, sender, null, null, null);
            } else if (args[0].equalsIgnoreCase("stats")) {
                commandStats(world, sender);
            } else if (args[0].equalsIgnoreCase("setspawn")) {
                commandSetSpawn(world, sender);
            } else if (args[0].equalsIgnoreCase("biomeshow")) {
                commandBiomeShow(world, sender);
            } else if (args[0].equalsIgnoreCase("biomelist")) {
                commandBiomeList(world, sender);
            } else if (args[0].equalsIgnoreCase("calc")) {
                commandCalc(world, sender, null);
            } else if (args[0].equalsIgnoreCase("rank")) {
                commandRank(world, sender);
            } else if (args[0].equalsIgnoreCase("lock")) {
                commandLock(world, sender, null);
            } else if (args[0].equalsIgnoreCase("unlock")) {
                commandUnLock(world, sender, null);
            } else if (args[0].equalsIgnoreCase("biomeset")) {
                commandBiomeSet(world, sender, null, null);
            } else if (args[0].equalsIgnoreCase("sendhome")) {
                commandSendHome(world, sender, null);
            } else if (args[0].equalsIgnoreCase("sendallhome")) {
                commandSendAllHome(world, sender, null);
            } else if (args[0].equalsIgnoreCase("add")) {
                commandAdd(world, sender, null, null);
            } else if (args[0].equalsIgnoreCase("remove")) {
                commandRemove(world, sender, null);
            } else if (args[0].equalsIgnoreCase("checkpos")) {
                commandCheckPos(world, sender, null);
            } else if (args[0].equalsIgnoreCase("createspawn")) {
                commandCreateSpawn(world, sender);
            } else if (args[0].equalsIgnoreCase("purgeprotect")) {
                commandPurgeProtect(world, sender, null);
            } else if (args[0].equalsIgnoreCase("visitblock")) {
                commandVisitBlock(world, sender, null);
            } else if (args[0].equalsIgnoreCase("visitallow")) {
                commandVisitAllow(world, sender, null);
            } else if (args[0].equalsIgnoreCase("clearspacing")) {
                commandClearSpacing(world, sender, null);
            } else if (args[0].equalsIgnoreCase("clearcreatelimit")) {
                commandClearCreateLimit(world, sender, null);
            } else if (args[0].equalsIgnoreCase("extend")) {
                commandExtend(world, sender, null);
            } else if (args[0].equalsIgnoreCase("changeowner")) {
                commandChangeOwner(world, sender, null, null);
            } else if (args[0].equalsIgnoreCase("makebackup")) {
                commandMakeBackup(world, sender);
            } else if (args[0].equalsIgnoreCase("removepoints")) {
                commandRemovePoints(world, sender, null);
            } else
                showUsage(sender);
        } else if (args.length == 2) {
            if (args[0].equalsIgnoreCase("info")) {
                commandInfo(world, sender, args[1]);
            } else if (args[0].equalsIgnoreCase("delete")) {
                commandDelete(world, sender, args[1]);
            } else if (args[0].equalsIgnoreCase("tp")) {
                commandTeleport(world, sender, args[1]);
            } else if (args[0].equalsIgnoreCase("sethome")) {
                commandSetHome(world, sender, args[1]);
            } else if (args[0].equalsIgnoreCase("fixhome")) {
                commandFixHome(world, sender, args[1]);
            } else if (args[0].equalsIgnoreCase("purge")) {
                commandPurge(world, sender, args[1], null);
            } else if (args[0].equalsIgnoreCase("challenges")) {
                commandChallenges(world, sender, args[1], "");
            } else if (args[0].equalsIgnoreCase("chadel")) {
                commandChallengesDelete(world, sender, args[1]);
            } else if (args[0].equalsIgnoreCase("create")) {
                commandCreate(world, sender, args[1], null, null);
            } else if (args[0].equalsIgnoreCase("calc")) {
                commandCalc(world, sender, args[1]);
            } else if (args[0].equalsIgnoreCase("lock")) {
                commandLock(world, sender, args[1]);
            } else if (args[0].equalsIgnoreCase("unlock")) {
                commandUnLock(world, sender, args[1]);
            } else if (args[0].equalsIgnoreCase("biomeset")) {
                commandBiomeSet(world, sender, null, null);
            } else if (args[0].equalsIgnoreCase("sendhome")) {
                commandSendHome(world, sender, args[1]);
            } else if (args[0].equalsIgnoreCase("sendallhome")) {
                commandSendAllHome(world, sender, args[1]);
            } else if (args[0].equalsIgnoreCase("add")) {
                commandAdd(world, sender, args[1], null);
            } else if (args[0].equalsIgnoreCase("remove")) {
                commandRemove(world, sender, args[1]);
            } else if (args[0].equalsIgnoreCase("checkpos")) {
                commandCheckPos(world, sender, args[1]);
            } else if (args[0].equalsIgnoreCase("purgeprotect")) {
                commandPurgeProtect(world, sender, args[1]);
            } else if (args[0].equalsIgnoreCase("visitblock")) {
                commandVisitBlock(world, sender, args[1]);
            } else if (args[0].equalsIgnoreCase("visitallow")) {
                commandVisitAllow(world, sender, args[1]);
            } else if (args[0].equalsIgnoreCase("clearspacing")) {
                commandClearSpacing(world, sender, args[1]);
            } else if (args[0].equalsIgnoreCase("clearcreatelimit")) {
                commandClearCreateLimit(world, sender, args[1]);
            } else if (args[0].equalsIgnoreCase("extend")) {
                commandExtend(world, sender, args[1]);
            } else if (args[0].equalsIgnoreCase("changeowner")) {
                commandChangeOwner(world, sender, args[1], null);
            } else if (args[0].equalsIgnoreCase("removepoints")) {
                commandRemovePoints(world, sender, args[1]);
            } else
                showUsage(sender);

        } else if (args.length == 3) {
            if (args[0].equalsIgnoreCase("challenges")) {
                commandChallenges(world, sender, args[1], args[2]);
            } else if (args[0].equalsIgnoreCase("create")) {
                commandCreate(world, sender, args[1], args[2], null);
            } else if (args[0].equalsIgnoreCase("purge")) {
                commandPurge(world, sender, args[1], args[2]);
            } else if (args[0].equalsIgnoreCase("biomeset")) {
                commandBiomeSet(world, sender, args[1], args[2]);
            } else if (args[0].equalsIgnoreCase("add")) {
                commandAdd(world, sender, args[1], args[2]);
            } else if (args[0].equalsIgnoreCase("changeowner")) {
                commandChangeOwner(world, sender, args[1], args[2]);
            } else
                showUsage(sender);
        } else if (args.length == 4) {
            if (args[0].equalsIgnoreCase("create")) {
                commandCreate(world, sender, args[1], args[2], args[3]);
            } else
                showUsage(sender);
        } else
            showUsage(sender);

    }

    private boolean commandCreateSpawn(World world, CommandSender sender) {
        if (!(sender.hasPermission("islandworld.islandev.createspawn")))
            return plugin.showError(sender, plugin.getLoc("error-no-perms"));

        for (int x = -3; x <= 3; x++) {
            for (int z = -3; z <= 3; z++) {
                final Block nb = plugin.getIslandWorld().getBlockAt(-100 + x, 150, -100 + z);
                if (nb != null)
                    nb.setType(Material.STONE);
            }
        }
        // Changing spawnpoint
        plugin.getIslandWorld().setSpawnLocation(-100, 151, -100);

        // Teleporting player
        if (sender instanceof Player) {

            ((Player) sender).teleport(new Location(plugin.getIslandWorld(), -100, 151, -100));
            ((Player) sender).sendMessage("Spawn created");
        }
        return false;
    }

    private boolean commandCreate(World world, CommandSender sender, String schemName, String who, String pos) {
        // if (!(player.hasPermission("islandworld.islandev.create")))
        // return plugin.showError(player, plugin.getLoc("error-no-perms"));

        if (schemName == null || schemName.isEmpty())
            return plugin.showError(sender, "You need provide schematic");

        if (who == null || who.isEmpty())
            return plugin.showError(sender, "You need provide owner");

        if (!(sender.hasPermission("islandworld.islandev.create." + schemName)))
            return plugin.showError(sender, plugin.getLoc("error-schematic").replaceAll("%name%", schemName));

        Player owner = Bukkit.getPlayer(who);
        if (owner != null) {
            if (plugin.haveIsland(owner))
                return plugin.showError(sender, plugin.getLoc("error-have-island"));
            if (plugin.isHelping(owner))
                return plugin.showError(sender, plugin.getLoc("error-helping"));
            if (WeApi.loadSchematic(plugin, schemName, sender) == null)
                return plugin.showError(sender, plugin.getLoc("error-schematic-not-exists").replaceAll("%name%", schemName));

            if (plugin.getFreeList() != null && !plugin.getFreeList().isEmpty()) {
                SimpleIsland newIsland = null;

                if (pos == null || pos.isEmpty()) {
                    newIsland = plugin.getFreeList().iterator().next();
                } else {
                    final List<SimpleIsland> freeList = plugin.getFreeList();
                    for (SimpleIsland is : freeList) {
                        if (is.getHash().equalsIgnoreCase(pos)) {
                            newIsland = is;
                            break;
                        }
                    }
                    if (newIsland == null)
                        return plugin.showError(sender, "Island on position " + pos + " is not on free list");
                }
                if (newIsland != null) {
                    // Create Island
                    plugin.onCreate(newIsland, owner, schemName);
                    // Send info
                    sender.sendMessage(ChatColor.YELLOW + plugin.getLoc("info-created"));
                    // Send info
                    owner.sendMessage(ChatColor.YELLOW + sender.getName() + " make island for you.");
                } else
                    sender.sendMessage(ChatColor.RED + plugin.getLoc("error-get-free"));
            } else
                sender.sendMessage(ChatColor.RED + plugin.getLoc("error-no-free"));
        } else
            return plugin.showError(sender, "Invalid player");

        return true;
    }

    private boolean commandChallenges(World world, CommandSender sender, String plName, String ff) {
        if (!(sender.hasPermission("islandworld.islandev.challenges")))
            return plugin.showError(sender, plugin.getLoc("error-no-perms"));

        int from = 0;
        if (plugin.isDigit(ff))
            from = Integer.valueOf(ff);

        if (plName.equalsIgnoreCase("")) {
            plugin.showMessage(sender, plugin.getLoc("com-usage"));
            sender.sendMessage(ChatColor.RED + "/islandev challenges <playerName> [page]" + ChatColor.AQUA + " - check player challenges\n");
            return false;
        }

        if (plugin.getOfflinePlayer(plName) != null) {
            if (plugin.isDigit(ff)) {
                plugin.showChallengeList(sender, plName.toLowerCase(), from);
            } else {
                final List<Challenge> list = plugin.getChallenges();
                int count = 0;
                for (Challenge cha : list) {
                    if (plugin.playerCompletedChallenge(plName, cha.getId()))
                        count++;
                }
                sender.sendMessage(ChatColor.AQUA + "Player " + plName + " challenges: " + count + "/" + list.size());
                sender.sendMessage(ChatColor.YELLOW + "Use page number for details");
                sender.sendMessage(ChatColor.RED + "/islandev challenges <playerName> [page]");
            }
        } else
            plugin.showError(sender, "Invalid playername");

        return true;
    }

    private boolean commandPurge(World world, CommandSender sender, String days, String howmuch) {
        if (!(sender.hasPermission("islandworld.islandev.purge")))
            return plugin.showError(sender, plugin.getLoc("error-no-perms"));

        if (!plugin.isDigit(days)) {
            plugin.showMessage(sender, plugin.getLoc("com-usage"));
            sender.sendMessage(ChatColor.RED + "/islandev purge <days>" + ChatColor.AQUA
                    + " - Removes islands that have not been visited in <days> days.\n");
            return false;
        }

        if (IslandWorld.purgeInProgress)
            return plugin.showError(sender, "Purge in progress");

        int hmuch = 0;
        if (plugin.isDigit(howmuch))
            hmuch = Integer.valueOf(howmuch);

        final int intdays = Integer.valueOf(days);
        if (intdays > 0)
            plugin.purgeIslands(sender, intdays, hmuch);
        else
            return plugin.showError(sender, "Days should be higher than 0");

        return true;
    }

    private boolean commandPurgeBreak(World world, CommandSender sender) {
        if (!(sender.hasPermission("islandworld.islandev.purgebreak")))
            return plugin.showError(sender, plugin.getLoc("error-no-perms"));

        if (!IslandWorld.purgeInProgress)
            return plugin.showError(sender, "Purge in not in progress");

        IslandWorld.breakPurge = true;

        return plugin.showMessage(sender, "Trying to break purge");
    }

    private boolean commandSetHome(World world, CommandSender sender, String who) {
        if (!(sender.hasPermission("islandworld.islandev.sethome")))
            return plugin.showError(sender, plugin.getLoc("error-no-perms"));

        if (sender instanceof Player) {
            Player player = (Player) sender;

            if (who == null || who.isEmpty()) {
                plugin.showMessage(player, plugin.getLoc("com-usage"));
                player.sendMessage(ChatColor.RED + "/islandev sethome <playerName>" + ChatColor.AQUA + " - Change home location on player island\n");
                return false;
            }

            if (plugin.getOfflinePlayer(who) != null) {
                SimpleIsland isle = plugin.getPlayerIsland(who);
                if (isle != null) {
                    if (plugin.isSafeToTeleport(new MyLocation(player.getLocation()))) {
                        final Location loc = player.getLocation();
                        // Set it
                        isle.setLocation(loc);
                        // Info
                        player.sendMessage("Location changed.");
                    } else
                        return plugin.showError(player, plugin.getLoc("error-sethome-loc"));
                } else
                    return plugin.showError(player, "Player " + who + " doesn't have island");
            } else
                return plugin.showError(player, "Player " + who + " doesn't exists");
        } else
            sender.sendMessage("Cannot use that command from console!");

        return true;
    }

    private boolean commandFixHome(World world, CommandSender sender, String who) {
        if (!(sender.hasPermission("islandworld.islandev.fixhome")))
            return plugin.showError(sender, plugin.getLoc("error-no-perms"));

        if (who == null || who.isEmpty()) {
            plugin.showMessage(sender, plugin.getLoc("com-usage"));
            sender.sendMessage(ChatColor.RED + "/islandev fixhome <playerName>" + ChatColor.AQUA + " - Change home location on player island\n");
            return false;
        }

        if (plugin.getOfflinePlayer(who) != null) {
            SimpleIsland isle = plugin.getPlayerIsland(who);
            if (isle != null) {
                if (plugin.findIslandSpawn(isle, sender))
                    sender.sendMessage("Fixed home coords");
                else
                    sender.sendMessage("There is problem with coords, cannot find safe point");
            } else
                return plugin.showError(sender, "Player " + who + " doesn't have island");
        } else
            return plugin.showError(sender, "Player " + who + " doesn't exists");

        return true;
    }

    private boolean commandTeleport(World world, CommandSender sender, String who) {
        if (!(sender.hasPermission("islandworld.islandev.tp")))
            return plugin.showError(sender, plugin.getLoc("error-no-perms"));

        if (sender instanceof Player) {
            Player player = (Player) sender;

            if (who == null || who.isEmpty()) {
                plugin.showMessage(player, plugin.getLoc("com-usage"));
                player.sendMessage(ChatColor.RED + "/islandev tp <playerName>" + ChatColor.AQUA + " - Teleport to player island\n");
                return false;
            }

            if (plugin.getOfflinePlayer(who) != null) {
                SimpleIsland isle = plugin.getPlayerIsland(who);
                if (isle == null)
                    isle = plugin.getHelpingIsland(who);

                if (isle != null) {
                    final MyLocation loc = isle.getLocation();
                    if (loc != null) {
                        // Teleport
                        plugin.teleportPlayer(plugin.getIslandWorld(), player, isle.getLocation());
                        // All done
                        player.sendMessage(ChatColor.YELLOW + "Teleported to " + who + " island");
                    } else
                        return plugin.showError(player, "Island doesnt have spawn location set!");
                } else
                    return plugin.showError(player, "Player " + who + " doesn't have island");
            } else
                return plugin.showError(player, "Player " + who + " doesn't exists");
        } else
            sender.sendMessage("Cannot use that command from console!");

        return true;
    }

    private boolean commandDelete(World world, CommandSender sender, String who) {
        if (!(sender.hasPermission("islandworld.islandev.delete")))
            return plugin.showError(sender, plugin.getLoc("error-no-perms"));

        if (who == null || who.isEmpty()) {
            plugin.showMessage(sender, plugin.getLoc("com-usage"));
            sender.sendMessage(ChatColor.RED + "/islandev delete <playerName>" + ChatColor.AQUA + " - Delete players island\n");
            return false;
        }

        if (plugin.getOfflinePlayer(who) != null) {
            SimpleIsland isle = plugin.getPlayerIsland(who);
            if (isle != null) {
                // Teleport all player outside
                final Collection<? extends Player> players = Bukkit.getOnlinePlayers();
                for (Player pl : players) {
                    if (pl != null && plugin.isInsideIsland(pl, isle)) {
                        plugin.teleToSpawn(pl);
                    }
                }
                // Delete island
                plugin.onDelete(isle, who);
                // All done
                sender.sendMessage(ChatColor.YELLOW + "Island deleted");
            } else
                return plugin.showError(sender, "Player " + who + " doesn't have island");
        } else
            return plugin.showError(sender, "Player " + who + " doesn't exists");

        return true;
    }

    private boolean commandInfo(World world, CommandSender sender, String who) {
        if (!(sender.hasPermission("islandworld.islandev.info")))
            return plugin.showError(sender, plugin.getLoc("error-no-perms"));

        if (who == null || who.isEmpty()) {
            if (sender instanceof Player) {
                boolean found = false;

                final HashMap<String, SimpleIsland> list = plugin.getIsleList();
                final Location curr = ((Player) sender).getLocation();

                final int isX = curr.getBlockX() / Config.ISLE_SIZE;
                final int isZ = curr.getBlockZ() / Config.ISLE_SIZE;

                for (Entry<String, SimpleIsland> e : list.entrySet()) {

                    final String ownerName = e.getKey();
                    final SimpleIsland island = e.getValue();
                    if (island.getX() == isX && island.getZ() == isZ) {
                        plugin.showInfo(sender, ownerName, island);
                        found = true;
                        break;
                    }
                }
                if (!found)
                    plugin.showError(sender, "You are not inside any island");

                return true;
            }
        } else {
            if (plugin.getOfflinePlayer(who) != null) {
                SimpleIsland isle = plugin.getPlayerIsland(who);
                if (isle == null && plugin.isHelping(who))
                    isle = plugin.getHelpingIsland(who);

                if (isle != null) {
                    plugin.showInfo(sender, who, isle);
                } else
                    return plugin.showError(sender, "Player " + who + " doesn't have island or help on island.");
            } else
                return plugin.showError(sender, "Player " + who + " doesn't exists");
        }

        return true;
    }

    private boolean commandReload(World world, CommandSender sender) {
        if (!(sender.hasPermission("islandworld.islandev.reload")))
            return plugin.showError(sender, plugin.getLoc("error-no-perms"));

        plugin.reloadConfig();
        Config cfg = new Config(plugin);
        cfg.setupDefaults();

        plugin.loadMessages();

        sender.sendMessage(ChatColor.YELLOW + "Configuration reloaded");

        return true;
    }

    private boolean commandUnload(World world, CommandSender sender) {
        if (!(sender.hasPermission("islandworld.islandev.unload")))
            return plugin.showError(sender, plugin.getLoc("error-no-perms"));

        World w = world;

        if (sender instanceof Player)
            w = ((Player) sender).getWorld();

        if (w != null) {
            int un = 0;
            boolean result = false;
            for (Chunk ch : w.getLoadedChunks()) {
                result = ch.unload();
                if (result)
                    un++;
            }

            sender.sendMessage(ChatColor.YELLOW + "Unloaded " + un + " chunks");
        }
        return true;
    }

    private boolean commandSave(World world, CommandSender sender) {
        if (!(sender.hasPermission("islandworld.islandev.save")))
            return plugin.showError(sender, plugin.getLoc("error-no-perms"));

        try {
            plugin.saveDatFiles();
        } catch (Exception e) {
            e.printStackTrace();
        }

        sender.sendMessage(ChatColor.YELLOW + "Saved dat files");

        return true;
    }

    private boolean commandClearDrop(World world, CommandSender sender) {
        if (!(sender.hasPermission("islandworld.islandev.cleardrop")))
            return plugin.showError(sender, plugin.getLoc("error-no-perms"));

        int count = 0;
        for (Entity i : world.getEntities()) {
            if (i != null && i.getType() == EntityType.DROPPED_ITEM) {
                i.remove();
                count++;
            }
        }
        plugin.showError(sender, "Removed " + count + " items from ground");

        return true;
    }

    private boolean commandStats(World world, CommandSender sender) {
        if (!(sender.hasPermission("islandworld.islandev.stats")))
            return plugin.showError(sender, plugin.getLoc("error-no-perms"));

        plugin.showStats(sender);

        return true;
    }

    private boolean commandSetSpawn(World world, CommandSender sender) {
        if (!(sender.hasPermission("islandworld.islandev.setspawn")))
            return plugin.showError(sender, plugin.getLoc("error-no-perms"));

        if (sender instanceof Player) {
            Player player = (Player) sender;

            final World w = player.getWorld();
            final Location l = player.getLocation();

            w.setSpawnLocation(l.getBlockX(), l.getBlockY(), l.getBlockZ());

            plugin.getConfig().set("spawn.yaw", l.getYaw());
            plugin.getConfig().set("spawn.pitch", l.getPitch());
            plugin.saveConfig();

            player.sendMessage(ChatColor.AQUA + "Changed " + w.getName() + " spawn location to: " + l.getBlockX() + ", " + l.getBlockY() + ", " + l.getBlockZ());
        } else
            sender.sendMessage("Cannot use that command from console!");

        return true;
    }

    private boolean commandVisitClear(World world, CommandSender sender) {
        if (!(sender.hasPermission("islandworld.islandev.visitclear")))
            return plugin.showError(sender, plugin.getLoc("error-no-perms"));

        plugin.getVisitList().clear();
        sender.sendMessage(ChatColor.AQUA + "Visitlist cleared.");

        return true;
    }

    private boolean commandChallengesClear(World world, CommandSender sender) {
        if (!(sender.hasPermission("islandworld.islandev.chaclear")))
            return plugin.showError(sender, plugin.getLoc("error-no-perms"));

        plugin.getCompletedChallenges().clear();
        sender.sendMessage(ChatColor.AQUA + "Completed challenges cleared.");

        return true;
    }

    private boolean commandChallengesDelete(World world, CommandSender sender, String who) {
        if (!(sender.hasPermission("islandworld.islandev.chadel")))
            return plugin.showError(sender, plugin.getLoc("error-no-perms"));

        if (who == null || who.isEmpty()) {
            plugin.showMessage(sender, plugin.getLoc("com-usage"));
            sender.sendMessage(ChatColor.RED + "/islandev chadel <playerName>" + ChatColor.AQUA + " - Delete player's completed challenges\n");
            return false;
        }
        plugin.deleteChallenges(who);
        sender.sendMessage(ChatColor.AQUA + "Completed challenges cleared.");

        return true;
    }

    private boolean commandBiomeShow(World world, CommandSender sender) {
        if (!(sender.hasPermission("islandworld.islandev.biomeshow")))
            return plugin.showError(sender, plugin.getLoc("error-no-perms"));

        if (sender instanceof Player) {
            Player player = (Player) sender;

            Biome bio = player.getWorld().getBiome(player.getLocation().getBlockX(), player.getLocation().getBlockZ());
            if (bio != null)
                player.sendMessage(ChatColor.AQUA + "Biome at your location: " + ChatColor.YELLOW + bio.toString());

        } else
            sender.sendMessage("Cannot use that command from console!");

        return true;
    }

    private boolean commandMakeBackup(World world, CommandSender sender) {
        if (!(sender.hasPermission("islandworld.islandev.makebackup")))
            return plugin.showError(sender, plugin.getLoc("error-no-perms"));

        plugin.saveBackupFile(sender);

        return true;
    }

    private boolean commandBiomeList(World world, CommandSender sender) {
        if (!(sender.hasPermission("islandworld.islandev.biomelist")))
            return plugin.showError(sender, plugin.getLoc("error-no-perms"));

        Biome[] bm = Biome.values();

        sender.sendMessage(ChatColor.AQUA + "Biomes:");
        sender.sendMessage(ChatColor.GREEN + "" + StringUtils.join(bm, ", "));

        return true;
    }

    private boolean commandBiomeSet(World world, CommandSender sender, String owner, String biome) {
        if (!(sender.hasPermission("islandworld.islandev.biomeset")))
            return plugin.showError(sender, plugin.getLoc("error-no-perms"));

        if (plugin.getOfflinePlayer(owner) != null) {
            SimpleIsland isle = plugin.getPlayerIsland(owner);
            if (isle != null) {
                Biome newbiome = null;

                Biome[] bm = Biome.values();

                for (Biome b : bm) {
                    if (b.toString().equalsIgnoreCase(biome))
                        newbiome = b;
                }

                if (newbiome != null) {
                    int x = isle.getX() * Config.ISLE_SIZE;
                    int z = isle.getZ() * Config.ISLE_SIZE;

                    for (int x_operate = x; x_operate < (x + Config.ISLE_SIZE); x_operate++) {
                        for (int z_operate = z; z_operate < (z + Config.ISLE_SIZE); z_operate++) {
                            plugin.getIslandWorld().setBiome(x_operate, z_operate, newbiome);
                            plugin.showMessage(sender, "Biome set to " + newbiome.toString());
                        }
                    }
                } else
                    plugin.showError(sender, "Incorrect biome");

            } else
                return plugin.showError(sender, "Player " + owner + " doesn't have island");
        } else
            return plugin.showError(sender, "Player " + owner + " doesn't exists");

        return false;
    }

    private boolean commandCalc(World world, CommandSender sender, String who) {
        if (!(sender.hasPermission("islandworld.islandev.calc")))
            return plugin.showError(sender, plugin.getLoc("error-no-perms"));

        if (who == null || who.isEmpty())
            who = sender.getName();

        if (plugin.getOfflinePlayer(who) != null) {
            SimpleIsland isle = plugin.getPlayerIsland(who);
            if (isle == null)
                isle = plugin.getHelpingIsland(who);

            if (isle != null) {
                long points = plugin.calcIslandPoints(isle);

                String str = plugin.getLoc("info-for-points");
                str = str.replaceAll("%name%", who);
                str = str.replaceAll("%points%", String.valueOf(points));

                sender.sendMessage(ChatColor.AQUA + str);

                plugin.storePoints(isle.getOwner(), isle.getOwnerUUID(), points);
            } else
                return plugin.showError(sender, "Player " + who + " doesn't have island");
        } else
            return plugin.showError(sender, "Player " + who + " doesn't exists");

        return true;
    }

    private boolean commandRank(World world, CommandSender sender) {
        if (!(sender.hasPermission("islandworld.islandev.rank")))
            return plugin.showError(sender, plugin.getLoc("error-no-perms"));

        plugin.readPoints();

        if (plugin.getRankLastUpdate() <= 0)
            return plugin.showError(sender, plugin.getLoc("error-rank-not-updated"));
        else
            sender.sendMessage(ChatColor.YELLOW + "Rank reloaded");

        return true;
    }

    private boolean commandLock(World world, CommandSender sender, String who) {
        if (!(sender.hasPermission("islandworld.islandev.lock")))
            return plugin.showError(sender, plugin.getLoc("error-no-perms"));

        if (who == null || who.isEmpty()) {
            plugin.showMessage(sender, plugin.getLoc("com-usage"));
            sender.sendMessage(ChatColor.RED + "/islandev lock <playerName>" + ChatColor.AQUA + " - Lock player island\n");
            return false;
        }

        if (plugin.getOfflinePlayer(who) != null) {
            SimpleIsland isle = plugin.getPlayerIsland(who);
            if (isle != null) {
                if (!isle.isLocked()) {
                    isle.setLocked(true);
                } else
                    return plugin.showError(sender, who + "`s island is already protected");
            } else
                return plugin.showError(sender, "Player " + who + " doesn't have island");
        } else
            return plugin.showError(sender, "Player " + who + " doesn't exists");

        return true;
    }

    private boolean commandUnLock(World world, CommandSender sender, String who) {
        if (!(sender.hasPermission("islandworld.islandev.unlock")))
            return plugin.showError(sender, plugin.getLoc("error-no-perms"));

        if (who == null || who.isEmpty()) {
            plugin.showMessage(sender, plugin.getLoc("com-usage"));
            sender.sendMessage(ChatColor.RED + "/islandev unlock <playerName>" + ChatColor.AQUA + " - Unlock player island\n");
            return false;
        }

        if (plugin.getOfflinePlayer(who) != null) {
            SimpleIsland isle = plugin.getPlayerIsland(who);
            if (isle != null) {
                if (isle.isLocked()) {
                    isle.setLocked(false);
                } else
                    return plugin.showError(sender, who + "`s island is not protected.");
            } else
                return plugin.showError(sender, "Player " + who + " doesn't have island");
        } else
            return plugin.showError(sender, "Player " + who + " doesn't exists");

        return true;
    }

    private boolean commandSendHome(World world, CommandSender sender, String val) {
        if (!(sender.hasPermission("islandworld.islandev.sendhome")))
            return plugin.showError(sender, plugin.getLoc("error-no-perms"));

        if (val == null || val.isEmpty()) {
            plugin.showMessage(sender, plugin.getLoc("com-usage"));
            sender.sendMessage(ChatColor.RED + "/islandev sendhome <player>" + ChatColor.AQUA + " - Send player to his island.\n");
            return false;
        }

        final Player owner = Bukkit.getPlayer(val);

        if (owner != null && owner.isOnline()) {
            SimpleIsland island = plugin.getPlayerIsland(owner);
            if (island == null)
                island = plugin.getHelpingIsland(owner);

            if (island != null) {
                MyLocation dest_loc = island.getLocation();
                if (dest_loc != null) {
                    plugin.teleportPlayer(plugin.getIslandWorld(), owner, dest_loc);
                    plugin.showMessage(owner, plugin.getLoc("info-admin-teleported"));

                    plugin.showMessage(sender, "Player teleported to his island");
                }
            } else
                sender.sendMessage(ChatColor.RED + "Player doesnt have island.");
        } else
            sender.sendMessage(ChatColor.RED + "Player not online");

        return true;
    }

    private boolean commandSendAllHome(World world, CommandSender sender, String val) {
        if (!(sender.hasPermission("islandworld.islandev.sendallhome")))
            return plugin.showError(sender, plugin.getLoc("error-no-perms"));

        if (val == null || val.isEmpty()) {
            plugin.showMessage(sender, plugin.getLoc("com-usage"));
            sender.sendMessage(ChatColor.RED + "/islandev sendallhome <owner>" + ChatColor.AQUA + " - Send all players to island.\n");
            return false;
        }

        final Player owner = Bukkit.getPlayer(val);

        if (owner != null && owner.isOnline()) {
            SimpleIsland island = plugin.getPlayerIsland(owner);
            if (island == null)
                island = plugin.getHelpingIsland(owner);

            if (island != null) {
                MyLocation dest_loc = island.getLocation();
                if (dest_loc != null) {
                    int count = 0;
                    plugin.teleportPlayer(plugin.getIslandWorld(), owner, dest_loc);
                    plugin.showMessage(owner, plugin.getLoc("info-admin-teleported"));
                    count++;

                    List<String> lista = island.getMembers();
                    if (lista != null && !lista.isEmpty()) {
                        for (String mem : lista) {
                            final Player member = Bukkit.getPlayer(mem);
                            if (member != null && member.isOnline()) {
                                plugin.teleportPlayer(plugin.getIslandWorld(), member, dest_loc);
                                plugin.showMessage(member, plugin.getLoc("info-admin-teleported"));
                                count++;
                            }
                        }
                    }

                    plugin.showMessage(sender, "Teleported " + count + " players to island.");
                }
            } else
                sender.sendMessage(ChatColor.RED + "Player doesnt have island.");
        } else
            sender.sendMessage(ChatColor.RED + "Player not online");

        return true;
    }

    private boolean commandAdd(World world, CommandSender sender, String owner, String player) {
        if (!(sender.hasPermission("islandworld.islandev.add")))
            return plugin.showError(sender, plugin.getLoc("error-no-perms"));

        if (owner == null || owner.isEmpty()) {
            plugin.showMessage(sender, plugin.getLoc("com-usage"));
            sender.sendMessage(ChatColor.RED + "/islandev add <owner> <player>" + ChatColor.AQUA + " - Add player to owner island.\n");
            return false;
        }
        if (player == null || player.isEmpty()) {
            plugin.showMessage(sender, plugin.getLoc("com-usage"));
            sender.sendMessage(ChatColor.RED + "/islandev add <owner> <player>" + ChatColor.AQUA + " - Add player to owner island.\n");
            return false;
        }
        if (plugin.haveIsland(player) || plugin.isHelping(player)) {
            return plugin.showError(sender, "Player " + player + " have own island, or is already in party");
        }

        final SimpleIsland island = plugin.getPlayerIsland(owner);
        if (island != null) {
            island.addMember(player.toLowerCase());
            plugin.setIsHelping(player.toLowerCase(), island);

            final Player np = Bukkit.getPlayer(player);
            if (np != null)
                np.sendMessage(plugin.getLoc("info-party-joined-admin-player").replaceAll("%name%", owner));

            final Player ow = Bukkit.getPlayer(owner);
            if (ow != null)
                ow.sendMessage(plugin.getLoc("info-party-joined-admin-owner").replaceAll("%name%", player));
        } else
            return plugin.showError(sender, "Player " + owner + " doesnt have island.");

        return true;
    }


    private boolean commandRemove(World world, CommandSender sender, String val) {
        if (!(sender.hasPermission("islandworld.islandev.add")))
            return plugin.showError(sender, plugin.getLoc("error-no-perms"));

        if (val == null || val.isEmpty()) {
            plugin.showMessage(sender, plugin.getLoc("com-usage"));
            sender.sendMessage(ChatColor.RED + "/islandev remove <player>" + ChatColor.AQUA + " - Remove player from party.\n");
            return false;
        }

        final SimpleIsland island = plugin.getHelpingIsland(val);
        if (island != null) {
            if (island.isMember(val)) {
                island.removeMember(val.toLowerCase());
                plugin.removeHelping(val.toLowerCase());

                final Player owner = Bukkit.getPlayer(island.getOwner());
                if (owner != null)
                    owner.sendMessage(ChatColor.YELLOW + plugin.getLoc("info-remove-admin").replaceAll("%name%", val));

                final Player friend = Bukkit.getPlayer(val);
                if (friend != null && plugin.isInsideIslandCuboid(friend, island)) {
                    // Teleport player
                    plugin.teleToSpawn(friend);
                    // Send message
                    friend.sendMessage(ChatColor.YELLOW + plugin.getLoc("info-removed-admin"));
                }
            } else
                return plugin.showError(sender, "Player " + val + " is not member of party");
        } else
            return plugin.showError(sender, "Player " + val + " is not in party");

        return true;
    }

    private boolean commandCheckPos(World world, CommandSender sender, String val) {
        if (!(sender.hasPermission("islandworld.islandev.checkpos")))
            return plugin.showError(sender, plugin.getLoc("error-no-perms"));

        if (val == null || val.isEmpty()) {
            plugin.showMessage(sender, plugin.getLoc("com-usage"));
            sender.sendMessage(ChatColor.RED + "/islandev checkpos <player>" + ChatColor.AQUA + " - Check player position.\n");
            return false;
        }

        final Player owner = Bukkit.getPlayer(val);
        if (owner != null && owner.isOnline()) {
            if (plugin.haveIsland(owner)) {
                final SimpleIsland is = plugin.getPlayerIsland(owner);
                if (is != null) {
                    if (plugin.isInsideIsland(owner, is))
                        plugin.showMessage(sender, "Player is inside own island");
                    else
                        plugin.showError(sender, "Player is outside own island");
                } else
                    plugin.showError(sender, "Island null");
            } else
                plugin.showError(sender, "Player doesnt have own island");
        } else
            plugin.showError(sender, "Invalid player, or player not online");

        return true;
    }

    private boolean commandPurgeProtect(World world, CommandSender sender, String val) {
        if (!(sender.hasPermission("islandworld.islandev.add")))
            return plugin.showError(sender, plugin.getLoc("error-no-perms"));

        if (val == null || val.isEmpty()) {
            plugin.showMessage(sender, plugin.getLoc("com-usage"));
            sender.sendMessage(ChatColor.RED + "/islandev purgeprotect <player>" + ChatColor.AQUA + " - Player island will not purged.\n");
            return false;
        }

        final SimpleIsland is = plugin.getPlayerIsland(val);
        if (is != null) {
            boolean isPurged = !is.isPurgeProtected();
            is.setPurgeProtected(isPurged);

            if (isPurged)
                plugin.showMessage(sender, "Island is now protected");
            else
                plugin.showMessage(sender, "Player is now unprotected");
        } else
            plugin.showError(sender, "Invalid player, or player doesnt have island.");

        return true;
    }

    private boolean commandVisitBlock(World world, CommandSender sender, String val) {
        if (!(sender.hasPermission("islandworld.islandev.visitblock")))
            return plugin.showError(sender, plugin.getLoc("error-no-perms"));

        if (val == null || val.isEmpty()) {
            plugin.showMessage(sender, plugin.getLoc("com-usage"));
            sender.sendMessage(ChatColor.RED + "/islandev visitblock <player>" + ChatColor.AQUA + " - Block visits on player island.\n");
            return false;
        }

        final SimpleIsland is = plugin.getPlayerIsland(val);
        if (is != null) {
            if (!is.isVisitBlocked()) {
                is.setVisitBlocked(true);
                plugin.showMessage(sender, "Visits are blocked now");
            } else
                plugin.showError(sender, "Visits are blocked already");
        } else
            plugin.showError(sender, "Invalid player, or player doesnt have island.");

        return true;
    }

    private boolean commandVisitAllow(World world, CommandSender sender, String val) {
        if (!(sender.hasPermission("islandworld.islandev.visitallow")))
            return plugin.showError(sender, plugin.getLoc("error-no-perms"));

        if (val == null || val.isEmpty()) {
            plugin.showMessage(sender, plugin.getLoc("com-usage"));
            sender.sendMessage(ChatColor.RED + "/islandev visitallow <player>" + ChatColor.AQUA + " - Allow visits on player island.\n");
            return false;
        }

        final SimpleIsland is = plugin.getPlayerIsland(val);
        if (is != null) {
            if (is.isVisitBlocked()) {
                is.setVisitBlocked(false);
                plugin.showMessage(sender, "Visits are allowed now");
            } else
                plugin.showError(sender, "Visits are allowed already");
        } else
            plugin.showError(sender, "Invalid player, or player doesnt have island.");

        return true;
    }

    private boolean commandClearSpacing(World world, CommandSender sender, String who) {
        if (!(sender.hasPermission("islandworld.islandev.clearspacing")))
            return plugin.showError(sender, plugin.getLoc("error-no-perms"));

        if (who == null || who.isEmpty()) {
            plugin.showMessage(sender, plugin.getLoc("com-usage"));
            sender.sendMessage(ChatColor.RED + "/islandev clearspacing <playerName>" + ChatColor.AQUA + " - Clear spacing on player island\n");
            return false;
        }

        if (plugin.getOfflinePlayer(who) != null) {
            SimpleIsland isle = plugin.getPlayerIsland(who);
            if (isle != null) {
                int x = isle.getX() * Config.ISLE_SIZE;
                int z = isle.getZ() * Config.ISLE_SIZE;

                for (int y_operate = 256; y_operate > 0; y_operate--) {
                    for (int x_operate = x; x_operate < (x + Config.ISLE_SIZE); x_operate++) {
                        for (int z_operate = z; z_operate < (z + Config.ISLE_SIZE); z_operate++) {
                            final int rs = isle.getRegionSpacing();

                            final int px = x_operate;
                            final int pz = z_operate;

                            // Make math
                            int x1 = (isle.getX() * Config.ISLE_SIZE) + rs;
                            int z1 = (isle.getZ() * Config.ISLE_SIZE) + rs;

                            int x2 = ((isle.getX() * Config.ISLE_SIZE) + Config.ISLE_SIZE) - rs;
                            int z2 = ((isle.getZ() * Config.ISLE_SIZE) + Config.ISLE_SIZE) - rs;

                            if (!(px >= x1 && px < x2 && pz >= z1 && pz < z2)) {
                                Block block = world.getBlockAt(x_operate, y_operate, z_operate);

                                if (plugin.isOnIgnoreList(block))
                                    continue;

                                if (block != null && !block.isEmpty()) {
                                    block.getDrops().clear();

                                    // Clear blocks with inventory
                                    BlockState state = block.getState();
                                    if (state instanceof InventoryHolder) {
                                        InventoryHolder ih = (InventoryHolder) state;
                                        ih.getInventory().clear();
                                    }

                                    plugin.deleteBlock(block);
                                }
                            }
                        }
                    }
                }

                sender.sendMessage(ChatColor.YELLOW + "Spacing on player's island cleared.");
            } else
                return plugin.showError(sender, "Player " + who + " doesn't have island");
        } else
            return plugin.showError(sender, "Player " + who + " doesn't exists");

        return true;
    }

    private boolean commandClearCreateLimit(World world, CommandSender sender, String who) {
        if (!(sender.hasPermission("islandworld.islandev.clearcreatelimit")))
            return plugin.showError(sender, plugin.getLoc("error-no-perms"));

        if (who == null || who.isEmpty()) {
            plugin.showMessage(sender, plugin.getLoc("com-usage"));
            sender.sendMessage(ChatColor.RED + "/islandev clearcreatelimit <playerName>" + ChatColor.AQUA + " - Clear player create limit\n");
            return false;
        }

        final int limit = plugin.getCreateLimit(who.toLowerCase());
        if (limit > 0) {
            plugin.resetCreateLimit(who.toLowerCase());
            return plugin.showMessage(sender, "Create limit for player " + who + " removed.");
        } else
            return plugin.showError(sender, "Player " + who + " doesn't have create limit.");
    }

    private boolean commandExtend(World world, CommandSender sender, String who) {
        if (!(sender.hasPermission("islandworld.islandev.extend")))
            return plugin.showError(sender, plugin.getLoc("error-no-perms"));

        if (who == null || who.isEmpty()) {
            plugin.showMessage(sender, plugin.getLoc("com-usage"));
            sender.sendMessage(ChatColor.RED + "/islandev extend <playerName>" + ChatColor.AQUA + " - Extends player island\n");
            return false;
        }

        if (Config.REGION_STEP > 0) {
            if (plugin.getOfflinePlayer(who) != null) {
                SimpleIsland isle = plugin.getPlayerIsland(who);
                if (isle != null) {
                    final int newRegionSpacing = isle.getRegionSpacing() - Config.REGION_STEP;

                    if (newRegionSpacing > 0) {
                        isle.setRegionSpacing(newRegionSpacing);
                        return plugin.showMessage(sender, "Player " + who + "'s island extended, new region spacing value: " + newRegionSpacing);
                    } else
                        return plugin.showError(sender, "Error: You cannot extend " + who + "'s island more, current spacing: " + isle.getRegionSpacing() + ", step: " + Config.REGION_STEP);
                } else
                    return plugin.showError(sender, "Error: Player " + who + " doesn't have island");
            } else
                return plugin.showError(sender, "Error: Player " + who + " doesn't exists");
        } else
            return plugin.showError(sender, "Error: Region step = 0, extending disabled");
    }

    private boolean commandRemovePoints(World world, CommandSender sender, String who) {
        if (!(sender.hasPermission("islandworld.islandev.removepoints")))
            return plugin.showError(sender, plugin.getLoc("error-no-perms"));

        if (who == null || who.isEmpty()) {
            plugin.showMessage(sender, plugin.getLoc("com-usage"));
            sender.sendMessage(ChatColor.RED + "/islandev removepoints <playerName>" + ChatColor.AQUA + " - Remove player points from database\n");
            return false;
        }

        plugin.removePoints(who);

        return plugin.showMessage(sender, "Points removed");

    }

    private boolean commandChangeOwner(World world, CommandSender sender, String oldowner, String newowner) {
        if (!(sender.hasPermission("islandworld.islandev.changeowner")))
            return plugin.showError(sender, plugin.getLoc("error-no-perms"));

        if (oldowner != null && !oldowner.isEmpty() && newowner != null && !newowner.isEmpty()) {
            final SimpleIsland island = plugin.getPlayerIsland(oldowner);
            if (island != null) {
                if (!plugin.haveIsland(newowner)) {
                    if (!plugin.isHelping(newowner)) {
                        if (!plugin.isOnDeleteList(oldowner)) {
                            // Remove island from list
                            plugin.getIsleList().remove(oldowner);
                            // Change owner
                            island.setOwner(newowner);
                            // Add island into list
                            plugin.getIsleList().put(newowner.toLowerCase(), island);
                            // Save
                            try {
                                plugin.saveDatFiles();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            return plugin.showMessage(sender, "Owner changed from " + oldowner + " to " + newowner);
                        } else
                            return plugin.showError(sender, "Island is on delete list, cannot change owner");
                    } else
                        return plugin.showError(sender, "Player " + newowner + " is helping on some island");
                } else
                    return plugin.showError(sender, "Player " + newowner + " have already island.");
            } else
                return plugin.showError(sender, "Player " + oldowner + " doesn't have island.");
        } else {
            plugin.showMessage(sender, plugin.getLoc("com-usage"));
            sender.sendMessage(ChatColor.RED + "/islandev changeowner <oldOwner> <newOwner>" + ChatColor.AQUA + " - Change island owner to new one\n");
            return false;

        }
    }

    public void showUsage(CommandSender sender) {
        if (sender instanceof Player) {
            Player player = (Player) sender;

            // Player use only /island without params
            player.sendMessage(ChatColor.YELLOW + "Island World v" + plugin.getDescription().getVersion());
            player.sendMessage(ChatColor.AQUA + "Dev commands:");
            player.sendMessage(ChatColor.RED + "/islandev reload" + ChatColor.AQUA + " - reload configuration from file");
            player.sendMessage(ChatColor.RED + "/islandev save" + ChatColor.AQUA + " - save islands and world guard regions");
            player.sendMessage(ChatColor.RED + "/islandev createspawn" + ChatColor.AQUA + " - creates spawn point in island world");

            player.sendMessage(ChatColor.RED + "/islandev stats" + ChatColor.AQUA + " - show stats");
            player.sendMessage(ChatColor.RED + "/islandev visitclear" + ChatColor.AQUA + " - clear visit list cache");
            player.sendMessage(ChatColor.RED + "/islandev makebackup" + ChatColor.AQUA + " - make .dat file backup");

            player.sendMessage(ChatColor.RED + "/islandev info [playerName]" + ChatColor.AQUA + " - show info about player isle");
            player.sendMessage(ChatColor.RED + "/islandev create <schematic> <player> <pos>" + ChatColor.AQUA + " - create for player island using schematic");
            player.sendMessage(ChatColor.RED + "/islandev delete <playerName>" + ChatColor.AQUA + " - delete player island");
            player.sendMessage(ChatColor.RED + "/islandev tp <playerName>" + ChatColor.AQUA + " - teleport to player island");
            player.sendMessage(ChatColor.RED + "/islandev sethome <playerName>" + ChatColor.AQUA + " - change home location on player island");
            player.sendMessage(ChatColor.RED + "/islandev add <owner> <player>" + ChatColor.AQUA + " - Add player to owner island.\n");

            player.sendMessage(ChatColor.RED + "/islandev challenges <playerName> [page]" + ChatColor.AQUA + " - check player challenges");
            player.sendMessage(ChatColor.RED + "/islandev chadel <playerName>" + ChatColor.AQUA + " - Delete player's completed challenges\n");
            player.sendMessage(ChatColor.RED + "/islandev chaclear" + ChatColor.AQUA + " - Clear completed challenges for all players\n");

            player.sendMessage(ChatColor.RED + "/islandev purge <days> [mowmuch]" + ChatColor.AQUA + " - Removes islands that have not been visited in <days> days.");
            player.sendMessage(ChatColor.RED + "/islandev setspawn" + ChatColor.AQUA + " - Change world spawn location");
            player.sendMessage(ChatColor.RED + "/islandev cleardrop" + ChatColor.AQUA + " - Remove items dropped on ground");
            player.sendMessage(ChatColor.RED + "/islandev biomeshow" + ChatColor.AQUA + " - Show biome at your location");
            player.sendMessage(ChatColor.RED + "/islandev biomelist" + ChatColor.AQUA + " - List all possible biomes");
            player.sendMessage(ChatColor.RED + "/islandev biomeset <playerName> <biome>" + ChatColor.AQUA + " - Change biome on player's island");
            player.sendMessage(ChatColor.RED + "/islandev calc <playerName>" + ChatColor.AQUA + " - Calculate points for player");
            player.sendMessage(ChatColor.RED + "/islandev removepoints <playerName>" + ChatColor.AQUA + " - Remove player points from database\n");

            player.sendMessage(ChatColor.RED + "/islandev rank" + ChatColor.AQUA + " - Reload rank");
            player.sendMessage(ChatColor.RED + "/islandev lock <playerName>" + ChatColor.AQUA + " - Lock player island\n");
            player.sendMessage(ChatColor.RED + "/islandev unlock <playerName>" + ChatColor.AQUA + " - Unlock player island\n");
            player.sendMessage(ChatColor.RED + "/islandev sendhome <player>" + ChatColor.AQUA + " - Send player to his island.\n");
            player.sendMessage(ChatColor.RED + "/islandev sendallhome <owner>" + ChatColor.AQUA + " - Send all players to island.\n");

            player.sendMessage(ChatColor.RED + "/islandev rebuildpathway <playerName>" + ChatColor.AQUA + " - Rebuild pathway on player island\n");
            player.sendMessage(ChatColor.RED + "/islandev clearspacing <playerName>" + ChatColor.AQUA + " - Clear spacing on player island\n");
            player.sendMessage(ChatColor.RED + "/islandev clearcreatelimit <playerName>" + ChatColor.AQUA + " - Clear player create limit\n");

            player.sendMessage(ChatColor.RED + "/islandev purgeprotect <player>" + ChatColor.AQUA + " - Player island will not purged.\n");


            player.sendMessage(ChatColor.RED + "/islandev changeowner <oldOwner> <newOwner>" + ChatColor.AQUA + " - Change island owner to new one\n");
        }
    }
}