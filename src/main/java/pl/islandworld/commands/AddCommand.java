package pl.islandworld.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import pl.themolka.cmds.command.Command;
import pl.themolka.cmds.command.CommandException;

/**
 * Created by Artur on 07.09.2015.
 */
public class AddCommand extends Command {

    public AddCommand() {
        super(new String[]{"add", "dodaj"});
        super.setDescription("Dodaje gracza do wyspy.");
    }

    @Override
    public void handle(CommandSender sender, String label, String[] args) throws CommandException {
        StringBuilder sb = new StringBuilder();
        for (String s : args){
            sb.append(s);
            sb.append(" ");
        }
        Bukkit.getServer().dispatchCommand(sender, "is add " + sb.toString());
    }
}
