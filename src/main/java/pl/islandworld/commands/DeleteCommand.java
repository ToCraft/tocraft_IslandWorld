package pl.islandworld.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import pl.themolka.cmds.command.Command;
import pl.themolka.cmds.command.CommandException;

/**
 * Created by Artur on 07.09.2015.
 */
public class DeleteCommand extends Command {

    public DeleteCommand() {
        super(new String[]{"delete", "usun"});
        super.setDescription("Usun wyspe.");
    }

    @Override
    public void handle(CommandSender sender, String label, String[] args) throws CommandException {
        Bukkit.getServer().dispatchCommand(sender, "is delete");
    }
}
