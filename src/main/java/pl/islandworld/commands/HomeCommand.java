package pl.islandworld.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import pl.themolka.cmds.command.Command;
import pl.themolka.cmds.command.CommandException;

/**
 * Created by Artur on 07.09.2015.
 */
public class HomeCommand extends Command {

    public HomeCommand() {
        super(new String[]{"home", "dom", "h"});
        super.setDescription("Teleport na wyspe.");
    }

    @Override
    public void handle(CommandSender sender, String label, String[] args) throws CommandException {
        Bukkit.getServer().dispatchCommand(sender, "is home");
    }
}
