package pl.islandworld.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import pl.themolka.cmds.command.Command;
import pl.themolka.cmds.command.CommandException;

/**
 * Created by Artur on 07.09.2015.
 */
public class SetHomeCommand extends Command {

    public SetHomeCommand() {
        super(new String[]{"sethome", "ustawdom"});
        super.setDescription("Ustaw nowy dom.");
    }

    @Override
    public void handle(CommandSender sender, String label, String[] args) throws CommandException {
        Bukkit.getServer().dispatchCommand(sender, "is sethome");
    }
}
