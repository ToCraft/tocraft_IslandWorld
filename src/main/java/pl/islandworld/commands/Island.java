package pl.islandworld.commands;

import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import pl.islandworld.Config;
import pl.islandworld.IslandWorld;
import pl.islandworld.WeApi;
import pl.islandworld.entity.MyLocation;
import pl.islandworld.entity.SimpleIsland;
import pl.themolka.cmds.command.Command;
import pl.themolka.cmds.command.CommandException;
import pl.tocraft.core.CorePlugin;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;

/**
 * @author Gnacik & artur9010
 */
@SuppressWarnings("SpellCheckingInspection")
public class Island extends Command {
    private final IslandWorld plugin;

    private HashMap<String, Long> lastCommand = new HashMap<>();
    private HashMap<String, Long> lastCalc = new HashMap<>();

    public Island() {
        super(new String[]{"island", "is", "wyspa"});
        super.setDescription("Komendy wyspy");
        this.plugin = IslandWorld.getInstance();
    }

    private static Map<String, Integer> sortByComparator(Map<String, Integer> unsortMap, final boolean order) {

        List<Entry<String, Integer>> list = new LinkedList<>(unsortMap.entrySet());

        // Sorting the list based on values
        Collections.sort(list, (o1, o2) -> {
            if (order) {
                return o1.getValue().compareTo(o2.getValue());
            } else {
                return o2.getValue().compareTo(o1.getValue());

            }
        });

        // Maintaining insertion order with the help of LinkedList
        Map<String, Integer> sortedMap = new LinkedHashMap<>();
        for (Entry<String, Integer> entry : list) {
            sortedMap.put(entry.getKey(), entry.getValue());
        }

        return sortedMap;
    }

    @Override
    public void handle(CommandSender sender, String label, String[] args) throws CommandException {
        final Player player = (Player) sender;
        if (!IslandWorld.LOADED) {
            plugin.showError(player, plugin.getLoc("error-start"));
            plugin.showErrorMessage(player);
        }

        if (plugin.isOnDeleteList(player.getName().toLowerCase()) && args.length > 0 && !args[0].equalsIgnoreCase("delete"))
            plugin.showError(player, plugin.getLoc("error-delete-comm"));

        if (args.length == 0) {
            showUsage(player);
        } else if (args.length == 1) {
            switch(args[0]){
                case "create":
                    commandCreate(player, sender, null);
                    break;
                case "delete":
                    commandDelete(player);
                    break;
                case "info":
                    commandInfo(player, null);
                    break;
                case "home":
                    commandHome(player);
                    break;
                case "sethome":
                    commandSetHome(player);
                    break;
                case "expell":
                    commandExpell(player, null);
                    break;
                case "add":
                    commandAdd(player, null);
                    break;
                case "remove":
                    commandRemove(player, null);
                    break;
                case "leave":
                    commandLeave(player);
                    break;
                case "fixhome":
                    commandFixHome(player);
                    break;
                case "visit":
                    commandVisit(player, null);
                    break;
                case "join":
                    commandJoin(player);
                    break;
                case "calc":
                    commandCalc(player);
                    break;
                case "rank":
                    commandRank(player);
                    break;
                default:
                    showUsage(player);
            }
        } else if (args.length == 2) {
            //todo: switch
            if (args[1] != null && !args[1].isEmpty())
                args[1] = args[1].replaceAll("\\\\", "");

            if (args[0].equalsIgnoreCase("create")) {
                commandCreate(player, sender, args[1]);
            } else if (args[0].equalsIgnoreCase("add")) {
                commandAdd(player, args[1]);
            } else if (args[0].equalsIgnoreCase("remove")) {
                commandRemove(player, args[1]);
            } else if (args[0].equalsIgnoreCase("visit")) {
                commandVisit(player, args[1]);
            } else if (args[0].equalsIgnoreCase("expell")) {
                commandExpell(player, args[1]);
            } else if (args[0].equalsIgnoreCase("info")) {
                commandInfo(player, args[1]);
            } else
                showUsage(player);
        } else
            showUsage(player);
    }

    private boolean commandCreate(Player player, CommandSender sender, String schematicName) {
        if (player == null)
            return false;

        if (plugin.haveIsland(player)){
            CorePlugin.sendPrefixed(player, "Posiadasz juz wyspe.");
            return true;
        }

        if (plugin.isHelping(player)){
            CorePlugin.sendPrefixed(player, "Aktualnie jestes dodany do innej wyspy.");
            return true;
        }

        if (plugin.getFreeList() != null && !plugin.getFreeList().isEmpty()) {
            SimpleIsland newIsland = plugin.getFreeList().iterator().next();

            if (newIsland != null) {
                if (schematicName == null || schematicName.isEmpty()) {
                    schematicName = "normal";

                    @SuppressWarnings("unchecked") List<String> schematics = (List<String>) plugin.getConfig().getList("schematics");
                    if (schematics != null && !schematics.isEmpty()) {
                        for (String sche : schematics) {
                            if (player.hasPermission("islandworld.create." + sche)) {
                                schematicName = sche;
                                break;
                            }
                        }
                    }
                }

                if (!(player.hasPermission("islandworld.create." + schematicName))){
                    CorePlugin.sendPrefixed(player, "Brak uprawnien do utworzenia wyspy (schemat: " + schematicName + ")");
                    return true;
                }
                if (WeApi.loadSchematic(plugin, schematicName, sender) == null){
                    CorePlugin.sendPrefixed(player, "Schemat nie istnieje.");
                }

                // Create Island
                plugin.onCreate(newIsland, player, schematicName);
                // Send info
                CorePlugin.sendPrefixed(player, "Wyspa zostala utworzona!");
            }else{
                CorePlugin.sendPrefixed(player, "Brak wolnych wysp, zglos sie do administratora.");
            }
        }else{
            CorePlugin.sendPrefixed(player, "Brak wolnych wysp, zglos sie do administratora.");
        }

        return true;
    }

    private void commandDelete(Player player) {
        if(plugin.isHelping(player)){
            CorePlugin.sendPrefixed(player, "Aby usunac wyspe, musisz byc jej wlascicielem.");
            return;
        }
        if(!plugin.haveIsland(player)){
            CorePlugin.sendPrefixed(player, "Nie posiadasz wyspy.");
            return;
        }
        if(player.getWorld() != IslandWorld.getIslandWorld()){
            CorePlugin.sendPrefixed(player, "Aby usunac wyspe, musisz sie na niej znajdowac.");
            return;
        }

        if(plugin.isOnDeleteList(player.getName().toLowerCase())){
            SimpleIsland isle = plugin.getPlayerIsland(player);
            if(isle != null){
                CorePlugin.sendPrefixed(player, "Wyspa zostala usunieta.");
                Bukkit.getOnlinePlayers().stream().filter(pl -> pl != null && plugin.isInsideIsland(pl, isle)).forEach(plugin::teleToSpawn);
                plugin.removeFromDeleteList(player.getName().toLowerCase());
                lastCommand.put(player.getName().toLowerCase(), System.currentTimeMillis());
                plugin.onDelete(isle, player.getName().toLowerCase());
            }
        }else{
            plugin.addToDeleteList(player.getName().toLowerCase());
        }
        /*
        if (player.getWorld() != world){
            CorePlugin.sendPrefixed(player, "Ta komende mozesz wykonac tylko gdy jestes na wyspie.");
            return true;
        }

        if(plugin.isHelping(player)){
            CorePlugin.sendPrefixed(player, "Wyspa moze zostac usunieta tylko przez jej wlasciciela.");
            return true;
        }

        if (plugin.haveIsland(player)) {
            if (playerHaveCommandLimit(player))
                return plugin.showError(player, plugin.getLoc("error-delay").replaceAll("%time%", String.valueOf(Config.TIME_LIMIT)));

            if (plugin.isOnDeleteList(player.getName().toLowerCase())) {
                // Get correct isle
                final SimpleIsland isle = plugin.getPlayerIsland(player);
                if (isle != null) {
                    // All done
                    CorePlugin.sendPrefixed(player, "Wyspa zostala usunieta.");
                    // Teleport all player outside
                    final Collection<? extends Player> players = Bukkit.getOnlinePlayers();
                    players.stream().filter(pl -> pl != null && plugin.isInsideIsland(pl, isle)).forEach(plugin::teleToSpawn);
                    // Remove from list
                    plugin.removeFromDeleteList(player.getName().toLowerCase());
                    // Set limit
                    lastCommand.put(player.getName().toLowerCase(), System.currentTimeMillis());
                    // Delete island
                    plugin.onDelete(isle, player.getName().toLowerCase());
                } else{
                    CorePlugin.sendPrefixed(player, "Nie posiadasz wyspy lub nie jestes jej wlascicielem.");
                }
            } else
                plugin.addToDeleteList(player.getName().toLowerCase());
        } else
            CorePlugin.sendPrefixed(player, "Nie posiadasz wyspy.");

        return true;
        */
    }

    private void commandInfo(Player player, String other) {
        if(other == null){
            SimpleIsland isle = plugin.getPlayerIsland(player);
            if(isle == null){
                isle = plugin.getHelpingIsland(player);
            }
            if(isle == null){
                CorePlugin.sendPrefixed(player, "Nie posiadasz wyspy.");
                return;
            }
            plugin.showInfo(player, player.getName(), isle);
        }else{
            SimpleIsland isle = plugin.getPlayerIsland(other);
            if(isle == null){
                isle = plugin.getHelpingIsland(player);
            }
            if(isle == null){
                CorePlugin.sendPrefixed(player, "Taki gracz nie istnieje lub nie posiada wyspy.");
                return;
            }
            plugin.showInfo(player, other, isle);
        }

        /*
        SimpleIsland isle = plugin.getPlayerIsland(player);
        final SimpleIsland isleh = plugin.getHelpingIsland(player);
        if (isle == null && isleh != null)
            isle = isleh;

        if (isle != null)
            plugin.showInfo(player, player.getName(), isle);
        else
            CorePlugin.sendPrefixed(player, "Nie posiadasz wyspy.");

        return true;
        */
    }

    private void commandHome(Player player) {
        SimpleIsland isle = plugin.getPlayerIsland(player);
        final SimpleIsland isleh = plugin.getHelpingIsland(player);
        if (isle == null && isleh != null)
            isle = isleh;

        if (isle != null) {
            MyLocation dest_loc = isle.getLocation();
            if (!plugin.isSafeToTeleport(dest_loc)){
                CorePlugin.sendPrefixed(player, "Teleportacja nie jest bezpieczna. Wpisz /fixhome");
                return;
            }

            plugin.teleportPlayer(IslandWorld.getIslandWorld(), player, dest_loc);
            removeMobs(dest_loc);
            CorePlugin.sendPrefixed(player, "Witaj na wyspie!");
        } else
            CorePlugin.sendPrefixed(player, "Nie posiadasz wyspy.");
    }

    private void commandSetHome(Player player) {
        if(plugin.isHelping(player)){
            CorePlugin.sendPrefixed(player, "Tylko wlasciciel wyspy moze zmieniac polozenie domu.");
        }

        SimpleIsland isle = plugin.getPlayerIsland(player);

        if(isle != null){
            if(plugin.isInsideOwnIsland(player)){
                Location loc = player.getLocation();
                if(plugin.isSafeToTeleport(loc)){
                    isle.setLocation(loc);
                    CorePlugin.sendPrefixed(player, "Dom zostal ustawiony!");
                }else{
                    CorePlugin.sendPrefixed(player, "Nie mozesz ustawic domu w tym miejscu (niebezpieczne miejsce).");
                }
            }else{
                CorePlugin.sendPrefixed(player, "Musisz znajdowac sie na wyspie.");
            }
        }else{
            CorePlugin.sendPrefixed(player, "Nie posiadasz wyspy.");
        }
    }

    //todo: uzywanie funkcji z pluginu core
    private void commandExpell(Player player, String plName) {
        if(plugin.isHelping(player)){
            CorePlugin.sendPrefixed(player, "Tylko wlasciciel wyspy moze wykonac ta komende.");
            return;
        }

        SimpleIsland isle = plugin.getPlayerIsland(player);
        if (isle != null) {
            if (plName == null || plName.isEmpty()) {
                int count = plugin.expellPlayers(player, isle);

                if (count > 0)
                    plugin.showMessage(player, plugin.getLoc("info-expell").replaceAll("%count%", String.valueOf(count)));
                else
                    plugin.showMessage(player, plugin.getLoc("error-expell"));
            } else {
                Player pl = Bukkit.getPlayer(plName);
                if (pl != null) {
                    if (plugin.isInsideIslandCuboid(pl, isle)) {
                        if (pl.isOp() || pl.hasPermission("islandworld.bypass.expell")) {
                            plugin.showError(player, plugin.getLoc("info-expell-op").replaceAll("%name%", pl.getName()));
                            return;
                        }

                        plugin.showMessage(pl, plugin.getLoc("info-expelled").replaceAll("%name%", player.getName()));
                        plugin.teleToSpawn(pl);

                        plugin.showMessage(player, plugin.getLoc("info-expell").replaceAll("%count%", "1"));
                    } else
                        plugin.showMessage(player, plugin.getLoc("error-expell-outside"));
                } else
                    plugin.showMessage(player, plugin.getLoc("error-expell"));
            }
        } else
            plugin.showError(player, CorePlugin.getTag() + ChatColor.WHITE + "Nie posiadasz wyspy.");
    }

    //todo: uzywanie funkcji z pluginu core
    private void commandAdd(Player player, String plName) {
        if (plName == null || plName.isEmpty()) {
            plugin.showMessage(player, CorePlugin.getTag() + ChatColor.WHITE + "Poprawne uzycie:");
            plugin.showMessage(player, plugin.getLoc("com-command-add"));
            return;
        }

        SimpleIsland isle = plugin.getPlayerIsland(player);

        if(plugin.isHelping(player)){
            CorePlugin.sendPrefixed(player, "Tylko wlasciciel wyspy moze dodawac osoby.");
            return;
        }

        if (isle != null) {
            final Player friend = Bukkit.getPlayer(plName);
            if (friend != null && friend.isOnline() && player.canSee(friend)) {
                if (plugin.haveIsland(friend)){
                    CorePlugin.sendPrefixed(player, "Kolega posiada juz wyspe.");
                    return;
                }
                if (plugin.isHelping(friend)) {
                    CorePlugin.sendPrefixed(player, "Kolega jest juz dodany do jakiegos party.");
                    return;
                }
                if (plugin.getPartyList().containsKey(player.getName().toLowerCase())) {
                    plugin.showError(player, plugin.getLoc("error-party-visitor-wait"));
                    return;
                }

                if (plugin.getPartyList().containsValue(friend.getName().toLowerCase())){
                    plugin.showError(player, plugin.getLoc("error-party-owner-wait").replaceAll("%name%", friend.getName()));
                    return;
                }

                // Check
                final int pLimit = player.hasPermission("islandworld.vip.party") ? 25 : 10;
                // Members
                final List<String> members = isle.getMembers();
                if (members != null) {
                    if (members.size() >= pLimit){
                        plugin.showError(player, plugin.getLoc("error-add-limit"));
                        CorePlugin.sendPrefixed(player, "Party jest pelne. (max 10 osob, w przypadku kont VIP limit jest zwiekszony do 25)");
                        return;
                    }


                    if (!members.contains(plName.toLowerCase())) {
                        plugin.makePartyRequest(player, friend);

                        CorePlugin.sendPrefixed(player, "Gracz &6" + friend.getName() + " &fzostal zaproszony do party.");
                        //plugin.showMessage(player, plugin.getLoc("info-party-request").replaceAll("%name%", friend.getName()));
                        CorePlugin.sendPrefixed(friend, "Gracz &6" + player.getName() + " &fzaprosil Cie do party.");
                        CorePlugin.send(friend, "Aby zaakceptowac wpisz &3/is join&f.");
                        //plugin.showMessage(friend, plugin.getLoc("info-party-need-answer").replaceAll("%name%", player.getName()));
                    } else
                        CorePlugin.sendPrefixed(player, "Ten gracz nalezy juz do tego party.");
                } else
                    plugin.showError(player, plugin.getLoc("error-wg-region"));
            } else
                CorePlugin.sendPrefixed(player, "Gracz jest offline.");
        } else
            CorePlugin.sendPrefixed(player, "Nie posiadasz wyspy.");
    }

    //todo: uzywanie funkcji z pluginu core
    private boolean commandRemove(Player player, String plName) {
        if (plName == null || plName.isEmpty()) {
            plugin.showMessage(player, CorePlugin.getTag() + ChatColor.WHITE + "Poprawne uzycie:");
            plugin.showMessage(player, plugin.getLoc("com-command-remove"));
            return false;
        }

        SimpleIsland isle = plugin.getPlayerIsland(player);

        if(plugin.isHelping(player)){
            CorePlugin.sendPrefixed(player, "Tylko wlasciciel wyspy moze usuwac osoby.");
            return true;
        }

        if (isle != null) {
            final String friendName = plName.toLowerCase();
            // Check
            if (isle.isMember(friendName)) {
                isle.removeMember(friendName);
                plugin.showMessage(player, plugin.getLoc("info-remove-ok").replaceAll("%name%", friendName));
                plugin.removeHelping(friendName);
                final Player friend = Bukkit.getPlayer(friendName);
                if (friend != null && plugin.isInsideIslandCuboid(friend, isle)) {
                    plugin.teleToSpawn(friend);
                    plugin.showMessage(friend, plugin.getLoc("info-removed-ok").replaceAll("%name%", player.getName()));
                }
            } else
                plugin.showError(player, plugin.getLoc("error-remove-not-added").replaceAll("%name%", friendName));
        } else
            CorePlugin.sendPrefixed(player, "Nie posiadasz wyspy.");

        return false;
    }

    private void commandVisit(Player player, String plName) {
        if(!player.hasPermission("islandworld.visit")){
            CorePlugin.sendPrefixed(player, "&6Ta komenda wymaga aktywnego konta VIP.");
            return;
        }

        if(plName == null || plName.isEmpty()){
            CorePlugin.sendPrefixed(player, "Poprawne uzycie:");
            CorePlugin.send(player, "/is visit [nick]");
            return;
        }

        SimpleIsland isle = plugin.getPlayerIsland(plName);
        SimpleIsland isleh = plugin.getHelpingIsland(plName);

        if (isle == null && isleh != null)
            isle = isleh;

        if(isle != null){
            Player target = Bukkit.getPlayer(plName);
            if(target != null){
                if(targetIsSafePoint(IslandWorld.getIslandWorld(), isle)){
                    plugin.teleportPlayer(IslandWorld.getIslandWorld(), player, isle.getLocation());
                    CorePlugin.sendPrefixed(player, "Przeteleportowano na wyspe gracza " + target.getName());
                }else{
                    CorePlugin.sendPrefixed(player, "Nie mozna przeteleportowac. (popros wlasciciela wyspy o wpisanie /fixhome)");
                }
            }
        }else{
            CorePlugin.sendPrefixed(player, "Ten gracz nie posiada wyspy.");
        }


/*
        if (plName == null || plName.isEmpty()) {
            plugin.showMessage(player, CorePlugin.getTag() + ChatColor.WHITE + "Poprawne uzycie:");
            plugin.showMessage(player, plugin.getLoc("com-command-visit"));
            return;
        }

        if (player.getName().equalsIgnoreCase(plName)){
            plugin.showError(player, plugin.getLoc("error-visit-self"));
            return;
        }

        SimpleIsland isle = plugin.getPlayerIsland(plName);
        if (isle != null) {
            final Player target = Bukkit.getPlayer(plName);
            if (target != null) {
                if (targetIsSafePoint(IslandWorld.getIslandWorld(), isle)) {
                    plugin.teleportPlayer(IslandWorld.getIslandWorld(), player, isle.getLocation());
                    plugin.showMessage(player, plugin.getLoc("info-visit-visitor-open").replaceAll("%name%", isle.getOwner()));

                    final Player owner = Bukkit.getPlayer(isle.getOwner());
                    if (owner != null)
                        plugin.showMessage(owner, plugin.getLoc("info-visit-visitor-tp-open").replaceAll("%name%", player.getName()));
                } else {
                    plugin.showError(player, plugin.getLoc("error-visit-spawn").replaceAll("%name%", plName));
                }
            }
        } else {
            plugin.showError(player, plugin.getLoc("error-visit-no-island").replaceAll("%name%", plName));
        }*/
    }

    private void commandJoin(Player player) {
        final HashMap<String, String> tmpMap = new HashMap<>();
        tmpMap.putAll(plugin.getPartyList());

        for (Entry<String, String> x : tmpMap.entrySet()) {
            final String ow = x.getKey();
            final String vi = x.getValue();

            if (ow != null && vi != null && vi.equalsIgnoreCase(player.getName().toLowerCase())) {
                plugin.getPartyList().remove(ow);

                final SimpleIsland island = plugin.getPlayerIsland(ow);
                if (island != null) {
                    final Player visitor = Bukkit.getPlayer(vi);
                    if (visitor != null) {
                        island.addMember(visitor.getName().toLowerCase());
                        plugin.setIsHelping(visitor.getName(), island);

                        CorePlugin.sendPrefixed(visitor, "Dolaczyles do party.");
                        //plugin.showMessage(visitor, plugin.getLoc("info-party-joined").replaceAll("%name%", ow));
                        CorePlugin.sendPrefixed(Bukkit.getPlayer(ow), "Gracz &6" + visitor.getName() + " &fdolaczyl do party.");
                        //plugin.showMessage(Bukkit.getPlayer(ow), plugin.getLoc("info-party-joined-owner").replaceAll("%name%", visitor.getName()));
                    }
                }
            }
        }
    }

    private void commandLeave(Player player) {
        if(plugin.getPlayerIsland(player) != null){
            CorePlugin.sendPrefixed(player, "Jestes wlascicielem wyspy. Nie mozesz go opuscic, ale mozesz usunac komenda /delete.");
            return;
        }

        SimpleIsland island = plugin.getHelpingIsland(player);
        if (island != null) {
            plugin.removeHelping(player.getName());

            island.removeMember(player.getName());

            if (plugin.isInsideIslandCuboid(player, island))
                plugin.teleToSpawn(player);

            CorePlugin.sendPrefixed(player, "Opusciles party.");

            final Player owner = Bukkit.getPlayer(island.getOwner());
            if (owner != null)
                CorePlugin.sendPrefixed(owner, "Gracz &6" + player.getName() + " &fopuscil party.");
        } else
            CorePlugin.sendPrefixed(player, "Nie jestes dodany do zadnego party.");
    }

    private void commandFixHome(Player player) {
        SimpleIsland isle = plugin.getPlayerIsland(player);

        if (isle != null) {
            if (plugin.findIslandSpawn(isle, null))
                CorePlugin.sendPrefixed(player, "Poprawiono /home.");
            else
                CorePlugin.sendPrefixed(player, "Nie mozemy znalesc odpowiedniego miejsca na nowe /home.");
        } else
            CorePlugin.sendPrefixed(player, "Nie posiadasz wyspy lub nie jestes jej wlascicielem.");
    }

    private void commandCalc(Player player) {
        if(playerHaveCalcLimit(player)){
            CorePlugin.sendPrefixed(player, "Musisz odczekac 10 minut przed ponownym wykonaniem tej komendy.");
            return;
        }

        SimpleIsland isle = plugin.getPlayerIsland(player);

        if (isle != null) {
            lastCalc.put(player.getName().toLowerCase(), System.currentTimeMillis());

            long points = plugin.calcIslandPoints(isle);
            isle.setPoints(points);
            plugin.storePoints(isle.getOwner(), isle.getOwnerUUID(), points);

            CorePlugin.sendPrefixed(player, "Twoja wyspa zebrala &c" + points + "pkt&r.");
        } else {
            CorePlugin.sendPrefixed(player, "Nie posiadasz wyspy lub nie jestes jej wlascicielem.");
        }
    }

    private void commandRank(Player player) {
        if (System.currentTimeMillis() > (plugin.getRankLastUpdate() + (60000 * Config.RELOAD_TIME))) {
            plugin.readPoints();
        }
        if (plugin.getRankLastUpdate() <= 0){
            plugin.showError(player, plugin.getLoc("error-rank-not-updated"));
            return;
        }

        final String dateAsString = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(plugin.getRankLastUpdate());

        CorePlugin.sendHeader(player, "Najlepsze wyspy");

        Map<String, Integer> sortedMapAsc = sortByComparator(plugin.getPoints(), false);

        int c = 1;
        for (Entry<String, Integer> en : sortedMapAsc.entrySet()) {
            CorePlugin.send(player, "&7&o#" + c + " &3" + en.getKey() + " &8(&7" + en.getValue() + "&8)");
            c++;
            if(c == 10){
                break;
            }
        }

        CorePlugin.send(player, "&7Ostatnia aktualizacja rankingu: &c&o" + dateAsString);
        CorePlugin.send(player, "&7Aby obliczyc ilosc punktow dla swojej wyspy, wpisz &2/is calc&7.");
    }

    private boolean playerHaveCommandLimit(Player player) {
        if (Config.TIME_LIMIT > 0 && lastCommand.containsKey(player.getName().toLowerCase())) {
            long time = lastCommand.get(player.getName().toLowerCase());
            long diff = (System.currentTimeMillis() - time) / 1000;

            if (diff <= (Config.TIME_LIMIT * 60))
                return true;
        }
        return false;
    }

    private boolean playerHaveCalcLimit(Player player) {
        if (Config.CALC_LIMIT > 0 && lastCalc.containsKey(player.getName().toLowerCase())) {
            long time = lastCalc.get(player.getName().toLowerCase());
            long diff = (System.currentTimeMillis() - time) / 1000;

            if (diff <= (Config.CALC_LIMIT * 60))
                return true;
        }
        return false;
    }

    public void showUsage(Player player) {
        CorePlugin.sendHeader(player, "Komendy SkyBlock");
        CorePlugin.send(player, "&3/new &8- &7stworz nowa wyspe.");
        CorePlugin.send(player, "&3/delete &8- &7usun wyspe.");
        CorePlugin.send(player, "&3/is info &8- &7informacje na temat wyspy.");
        CorePlugin.send(player, "&3/home &8- &7teleportacja na wyspe.");
        CorePlugin.send(player, "&3/sethome &8- &7zmiana spawnu wyspy (/home).");
        CorePlugin.send(player, "&3/fixhome &8- &7przymusowa zmiana spawnu wyspy (jezeli niemozliwa jest teleportacja).");
        CorePlugin.send(player, "&3/is expell &8- &7usuwa \"niechcianych gosci\" z wyspy.");
        CorePlugin.send(player, "&3/add <nick> &8- &7dodaje gracza <nick> do party.");
        CorePlugin.send(player, "&3/is remove <nick> &8- &7usuwa gracza <nick> z party.");
        CorePlugin.send(player, "&3/leave &8- &7opuszcza party.");
        CorePlugin.send(player, "&3/is visit <nick> &8- &7teleportacja na wyspe gracza <nick>.");
        CorePlugin.send(player, "&3/is calc &8- &7\"wylicza\" punkty wyspy.");
        CorePlugin.send(player, "&3/ranking &8- &7ranking wysp.");
        CorePlugin.send(player, "&3/zadania list <strona> &8- &7spis zadan.");
        CorePlugin.send(player, "&3/zadania complete <id> &8- &7skoncz zadanie o numerze <id>.");
    }

    private boolean targetIsSafePoint(World world, SimpleIsland isle) {
        MyLocation l = isle.getLocation();

        Block above = world.getBlockAt(l.getBlockX(), l.getBlockY() + 1, l.getBlockZ());
        Block targe = world.getBlockAt(l.getBlockX(), l.getBlockY(), l.getBlockZ());
        Block under1 = world.getBlockAt(l.getBlockX(), l.getBlockY() - 1, l.getBlockZ());

        boolean piston = false;
        pistoncheck:
        {
            for (int x = -3; x <= 3; x++) {
                for (int y = -3; y <= 3; y++) {
                    for (int z = -3; z <= 3; z++) {
                        final Block nb = world.getBlockAt(targe.getX() + x, targe.getY() + y, targe.getZ() + z);

                        if (nb != null) {
                            switch (nb.getType()) {
                                case TRAP_DOOR:
                                case PISTON_BASE:
                                case PISTON_EXTENSION:
                                case PISTON_MOVING_PIECE:
                                case PISTON_STICKY_BASE:
                                    piston = true;
                                    break pistoncheck;
                                default:
                                    break;
                            }
                        }
                    }
                }
            }
        }

        return !piston && above.getType() == Material.AIR && targe.getType() == Material.AIR && under1.getType() != Material.AIR && under1.getType() != Material.LAVA && under1.getType() != Material.WATER && under1.getType() != Material.TRAP_DOOR;
    }

    private void removeMobs(MyLocation loc) {
        int px = loc.getBlockX();
        int py = loc.getBlockY();
        int pz = loc.getBlockZ();

        for (int x = -1; x <= 1; x++) {
            for (int z = -1; z <= 1; z++) {
                Chunk c = IslandWorld.getIslandWorld().getChunkAt(new Location(IslandWorld.getIslandWorld(), px + x * 16, py, pz + z * 16));
                for (Entity e : c.getEntities()) {
                    if (e != null && e instanceof Monster) {
                        e.remove();
                    }
                }
            }
        }
    }

}