package pl.islandworld.api;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import pl.islandworld.IslandWorld;
import pl.islandworld.entity.SimpleIsland;

/**
 * @author Gnacik
 */
public class IslandWorldApi {
    private static IslandWorld getPlugin() {
        return IslandWorld.getInstance();
    }

    /**
     * Function return true if IslandWorld plugin is correctly loaded and initialized
     *
     * @return true if everything is ok and you can use API methods
     */
    public static boolean isInitialized() {
        return (getPlugin() != null && getPlugin().isLoaded());
    }

    /**
     * Function checks if player have island (is owner)
     *
     * @param playername player name
     * @return True if player have island
     */
    public static boolean haveIsland(String playername) {
        return getPlugin().haveIsland(playername);
    }

    /**
     * Function checks if player is helping on island (is added to party)
     *
     * @param playername player name
     * @return True if player is helping
     */
    public static boolean isHelpingIsland(String playername) {
        return getPlugin().isHelping(playername);
    }

    /**
     * Function returns player points
     *
     * @param playername player name
     * @param helping    check also islands where player is helping (added to party)
     * @param recalc     if true points will be recalculated and new value will be returned
     * @return points
     */
    public static long getPoints(String playername, boolean helping, boolean recalc) {
        SimpleIsland is = getPlugin().getPlayerIsland(playername);
        if (is == null && helping)
            is = getPlugin().getHelpingIsland(playername);

        if (is != null) {
            if (recalc)
                return getPlugin().calcIslandPoints(is);
            else
                return is.getPoints();
        }
        return -1;
    }

    /**
     * Returns members of player island
     *
     * @param playername player name
     * @param helping    check also islands where player is helping (added to party)
     * @return String[] with members or null
     */
    public static String[] getMembers(String playername, boolean helping) {
        SimpleIsland is = getPlugin().getPlayerIsland(playername);
        if (is == null && helping)
            is = getPlugin().getHelpingIsland(playername);

        if (is != null) {
            return is.getMembers().toArray(new String[0]);
        }

        return null;
    }

    /**
     * Function returns island coords  - int[] {x, y}
     * If player doesnt have island it will return null
     *
     * @param playername player name
     * @param helping    check also islands where player is helping (added to party)
     * @return island coordinates
     */
    public static int[] getIslandCoords(String playername, boolean helping) {
        return getPlugin().getCoords(playername, helping);
    }

    /**
     * Checks if player is allowed to make action (build/break etc.) on location
     *
     * @param player  Player instance
     * @param loc     Location to check
     * @param helping check also islands where player is helping (added to party)
     * @return true if player is allowed
     */
    public static boolean canBuildOnLocation(Player player, Location loc, boolean helping) {
        return getPlugin().canBuildOnLocation(player, loc, helping);
    }
}