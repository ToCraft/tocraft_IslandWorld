package pl.islandworld.entity;

import org.bukkit.Location;
import org.bukkit.util.NumberConversions;

import java.io.Serializable;

public class MyLocation implements Serializable {
    private static final long serialVersionUID = 1L;
    private double x;
    private double y;
    private double z;
    private float pitch;
    private float yaw;

    public MyLocation(Location loc) {
        this(loc.getX(), loc.getY(), loc.getZ(), loc.getYaw(), loc.getPitch());
    }

    public MyLocation(final double x, final double y, final double z, final float yaw, final float pitch) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.pitch = pitch;
        this.yaw = yaw;
    }

    public static int locToBlock(double loc) {
        return NumberConversions.floor(loc);
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public int getBlockX() {
        return locToBlock(x);
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public int getBlockY() {
        return locToBlock(y);
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }

    public int getBlockZ() {
        return locToBlock(z);
    }

    public float getYaw() {
        return yaw;
    }

    public void setYaw(float yaw) {
        this.yaw = yaw;
    }

    public float getPitch() {
        return pitch;
    }

    public void setPitch(float pitch) {
        this.pitch = pitch;
    }

    public Location toLocation() {
        return new Location(null, x, y, z, pitch, yaw);
    }
}
