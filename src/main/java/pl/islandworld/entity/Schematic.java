package pl.islandworld.entity;

import com.sk89q.jnbt.Tag;
import org.bukkit.util.BlockVector;

import java.util.Map;

/*
* Copyright (C) 2012 p000ison
*
* This work is licensed under the Creative Commons
* Attribution-NonCommercial-NoDerivs 3.0 Unported License. To view a copy of
* this license, visit http://creativecommons.org/licenses/by-nc-nd/3.0/ or send
* a letter to Creative Commons, 171 Second Street, Suite 300, San Francisco,
* California, 94105, USA.
*
*/

/**
 * @author Max
 */
public class Schematic {
    private byte[] blocks;
    private byte[] data;
    private short width;
    private short lenght;
    private short height;
    private String fname;
    private Map<BlockVector, Map<String, Tag>> entities;

    public Schematic(byte[] blocks, byte[] data, Map<BlockVector, Map<String, Tag>> tileEntitiesMap, short width, short lenght, short height, String fname) {
        this.blocks = blocks;
        this.data = data;
        this.entities = tileEntitiesMap;
        this.width = width;
        this.lenght = lenght;
        this.height = height;
        this.fname = fname.replaceAll(".schematic", "");
    }

    /**
     * @return the blocks
     */
    public byte[] getBlocks() {
        return blocks;
    }

    /**
     * @return the data
     */
    public byte[] getData() {
        return data;
    }

    public Map<BlockVector, Map<String, Tag>> getEntities() {
        return entities;
    }

    /**
     * @return the width
     */
    public short getWidth() {
        return width;
    }

    /**
     * @return the lenght
     */
    public short getLenght() {
        return lenght;
    }

    /**
     * @return the height
     */
    public short getHeight() {
        return height;
    }

    /**
     * @return filename
     */
    public String getFileName() {
        return fname.toLowerCase();
    }
}
