package pl.islandworld.entity;

import org.bukkit.inventory.ItemStack;

import java.util.List;

public class Challenge {
    private int id;
    private String descr;
    private List<ItemStack> reqItems;
    private boolean reqTake = false;
    private List<ItemStack> rewItems;
    private boolean rep = false;

    public Challenge(int _id, String _descr, List<ItemStack> _req, boolean _take, List<ItemStack> _rew, boolean _rep) {
        id = _id;
        descr = _descr;
        reqItems = _req;
        reqTake = _take;
        rewItems = _rew;
        rep = _rep;
    }

    public int getId() {
        return id;
    }

    public String getDescr() {
        return descr;
    }

    public List<ItemStack> getRequiredItems() {
        return reqItems;
    }

    public boolean takeRequiredItems() {
        return reqTake;
    }

    public List<ItemStack> getRewardItems() {
        return rewItems;
    }

    public boolean isRepeatable() {
        return rep;
    }
}
