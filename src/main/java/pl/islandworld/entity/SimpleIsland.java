package pl.islandworld.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.stream.Collectors;

import org.bukkit.Location;

import pl.islandworld.Config;

/**
 * @author Gnacik
 */
@SuppressWarnings("SpellCheckingInspection")
public class SimpleIsland implements Serializable {
    private static final long serialVersionUID = 4L;

    private String owner;

    private int isle_x;
    private int isle_z;

    private MyLocation isle_loc;

    private boolean isLocked;

    private long createTime;
    private long ownerLoginTime;

    private String schematic;

    private ArrayList<String> members;
    private HashMap<String, MyLocation> homes;

    private long points;

    private boolean visitBlocked;

    private ArrayList<String> blockedPlayers;

    private boolean purgeProtected = false;

    private int regionSpacing;

    private UUID ownerUUID = null;

    public SimpleIsland(int x, int z) {
        isle_x = x;
        isle_z = z;

        createTime = System.currentTimeMillis();
        ownerLoginTime = System.currentTimeMillis();

        schematic = "normal";

        members = new ArrayList<>();
        blockedPlayers = new ArrayList<>();
        homes = new HashMap<>();

        regionSpacing = -1;
    }

    public SimpleIsland(SimpleIslandV6 old) {
        owner = old.getOwner();

        isle_x = old.getX();
        isle_z = old.getZ();

        isle_loc = old.getLocation();

        long cre = old.getCreateTime();
        if (cre == 0L)
            createTime = System.currentTimeMillis();
        else
            createTime = cre;

        long own = old.getOwnerLoginTime();
        if (own == 0L)
            ownerLoginTime = System.currentTimeMillis();
        else
            ownerLoginTime = own;

        schematic = old.getSchematic();

        members = new ArrayList<>();
        blockedPlayers = new ArrayList<>();

        List<String> oldm = old.getMembers();
        if ((oldm != null) && (!oldm.isEmpty())) {
            members.addAll(oldm.stream().filter(member -> (member != null) && (!member.isEmpty())).map(String::toLowerCase).collect(Collectors.toList()));
        }

        homes = new HashMap<>();

        if (!homes.isEmpty()){
            for (Entry<String, MyLocation> oh : old.getHomes().entrySet()) {
                homes.put(oh.getKey(), oh.getValue());
            }
        }

        points = 0L;

        regionSpacing = -1;
    }

    @Override
    public String toString() {
        return "[" + isle_x + "x" + isle_z + "][" + owner + "]";
    }

    public String getHash() {
        return isle_x + "x" + isle_z;
    }

    public void clear() {
        isle_loc = null;

        createTime = 0;
        ownerLoginTime = 0;

        schematic = null;

        members.clear();
        blockedPlayers.clear();
        homes.clear();
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String ow) {
        owner = ow;
    }

    public int getX() {
        return isle_x;
    }

    public int getZ() {
        return isle_z;
    }

    public void setLocation(Location loc) {
        isle_loc = new MyLocation(loc);
    }

    public MyLocation getLocation() {
        return isle_loc;
    }

    public void setCreateTime(long time) {
        createTime = time;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setOwnerLoginTime(long time) {
        ownerLoginTime = time;
    }

    public long getOwnerLoginTime() {
        return ownerLoginTime;
    }

    public void setSchematic(String schema) {
        schematic = schema;
    }

    public String getSchematic() {
        return schematic;
    }

    public List<String> getMembers() {
        return members;
    }

    public void addMember(String member) {
        members.add(member.toLowerCase());
    }

    public void removeMember(String member) {
        if (members.contains(member.toLowerCase()))
            members.remove(member.toLowerCase());
    }

    public boolean isMember(String name) {
        final List<String> list = getMembers();
        if (list != null && !list.isEmpty()) {
            for (String memb : list) {
                if (memb.equalsIgnoreCase(name))
                    return true;
            }
        }
        return false;
    }

    public boolean isLocked() {
        return isLocked;
    }

    public void setLocked(boolean state) {
        isLocked = state;
    }

    public void setPoints(long p) {
        points = p;
    }

    public long getPoints() {
        return points;
    }

    public boolean isVisitBlocked() {
        return visitBlocked;
    }

    public void setVisitBlocked(boolean val) {
        visitBlocked = val;
    }

    public ArrayList<String> getBlockedPlayers() {
        return blockedPlayers;
    }

    public boolean isPurgeProtected() {
        return purgeProtected;
    }

    public void setPurgeProtected(boolean purgeProtected) {
        this.purgeProtected = purgeProtected;
    }

    public int getRegionSpacing() {
        if (regionSpacing <= 0)
            return Config.REGION_SPACING;
        else
            return regionSpacing;
    }

    public void setRegionSpacing(int value) {
        regionSpacing = value;
    }

    public void setOwnerUUID(UUID uniqueId) {
        ownerUUID = uniqueId;
    }

    public UUID getOwnerUUID() {
        return ownerUUID;
    }
}