package pl.islandworld;

import java.util.List;

/**
 * @author Gnacik
 */
public class Config {
    public static int MAX_COUNT = 100;
    public static int ISLE_SIZE = 100;
    public static int ISLE_HEIGHT = 120;
    public static int REGION_SPACING = 3;
    public static int REGION_STEP = 0;
    public static int TIME_LIMIT = 10;
    public static int CALC_LIMIT = 10;
    public static int RELOAD_TIME = 30;
    public static int AUTO_PURGE = 0;
    public static int LAYER_DELAY = 5;
    public static int CREATE_LIMIT = 0;
    public static List<String> ALLOWED_ITEM_LIST;
    public static List<String> IGNORED_BLOCK_LIST;
    public static String mysql_host = "";
    public static String mysql_port = "";
    public static String mysql_user = "";
    public static String mysql_pass = "";
    public static String mysql_base = "";
    public static String mysql_table = "";
    private static IslandWorld plugin;

    public Config(IslandWorld plug) {
        plugin = plug;
    }

    @SuppressWarnings({"unchecked", "deprecation"})
    public void setupDefaults() {
        ALLOWED_ITEM_LIST = (List<String>) plugin.getConfig().getList("allowed-list");
        IGNORED_BLOCK_LIST = (List<String>) plugin.getConfig().getList("ignored-list");

        RELOAD_TIME = plugin.getConfig().getInt("reload-time", 30);
        if (RELOAD_TIME <= 1)
            RELOAD_TIME = 30;

        mysql_host = plugin.getConfig().getString("database.host", "localhost");
        mysql_port = plugin.getConfig().getString("database.port", "3306");
        mysql_user = plugin.getConfig().getString("database.user", "islandworld");
        mysql_pass = plugin.getConfig().getString("database.pass", "password");
        mysql_base = plugin.getConfig().getString("database.base", "islandworld");
        mysql_table = plugin.getConfig().getString("database.table", "island_stats");
    }
}