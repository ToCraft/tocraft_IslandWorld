package pl.islandworld;

import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldedit.schematic.SchematicFormat;
import org.bukkit.command.CommandSender;

import java.io.File;

/**
 * @author Gnacik
 */
public class WeApi {
    private static boolean setupWE(IslandWorld plugin) {
        WorldEditPlugin we = (WorldEditPlugin) plugin.getServer().getPluginManager().getPlugin("WorldEdit");

        return we != null;
    }

    public static SchematicFormat loadSchematic(IslandWorld plugin, String fName, CommandSender sender) {
        if (!setupWE(plugin)) {
            sender.sendMessage("ERROR: Problem with WE plugin");
            return null;
        }

        File schemaFile = new File(plugin.getDataFolder() + "/schematics/" + fName + ".schematic");
        if (schemaFile.exists()) {
            SchematicFormat schem = SchematicFormat.getFormat(schemaFile);
            if (schem == null)
                sender.sendMessage("ERROR: Invalid schematic format!");
            return schem;
        } else {
            sender.sendMessage("ERROR: Schematic doesnt exists: " + plugin.getDataFolder() + "/schematics/" + fName + ".schematic");
        }
        return null;
    }
}